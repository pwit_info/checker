

// Code written by Hans de Nivelle, April 2008.
// Formulas are used for both first-order formulas, and
// for schematic formulas.


#ifndef FORMULA_INCLUDED
#define FORMULA_INCLUDED    1


#include "term.h"


enum fol_oper
{
   fol_false, fol_true,

   fol_not,

   fol_and, fol_or,
   fol_implies, fol_equiv,

   fol_forall, fol_exists,

   fol_equals, fol_atom
}; 
	     


// A declaration declares a predicate or function variable 
/// without giving a value for it.
// A declaration is needed in quantifications.


struct declaration
{
   identifier var; 
   unsigned int arity;

   bool isfunction;


private: 
   declaration( identifier var, 
                unsigned int arity,
                bool isfunction )
   : var( var ),
     arity( arity ),
     isfunction( isfunction )
   { }


public: 
   static declaration make_funcdecl( identifier f, unsigned int arity );
   static declaration make_preddecl( identifier f, unsigned int arity );
/*static declaration make_funcdecl( std::string f, unsigned int arity );
   static declaration make_preddecl( std::string f, unsigned int arity );
*/
};
   bool operator== ( const declaration &d1, const declaration &d2);


std::ostream& operator << ( std::ostream& stream, 
                            const declaration& decl );




class formula
{

   struct formula_node
   {
      unsigned int reference_count;

      fol_oper op; 

      std::list< declaration > variables;
         // The quantified variables in case of fol_forall, fol_exists.

      std::list< identifier > pred;
	 // Empty list in all cases, except for fol_atom.
	 // In that case we have here a single predicate with which the
	 // atom is built.

      std::list< term > subterms;
         // Two subterms in case of fol_equals.
	 // An arbitrary number of subterms in case of fol_atom.
         // Otherwise empty.

      std::list< formula > subformulas;
         // 0 subformulas in case of fol_false, fol_true, fol_equals, 
	 //                          fol_atom.
	 // 1 subformula in case of fol_not, fol_forall, fol_exists.
	 // 2 subformulas in case of fol_implies, fol_equiv.
	 // Any number of subformulas distinct from 0 
	 // in case of fol_and, fol_or.


      formula_node( fol_oper op,
		    const std::list< declaration > & variables,
		    const std::list< identifier > & pred, 
		    const std::list< term > & subterms,
		    const std::list< formula > & subformulas ) 
         : op( op ),
           variables( variables ),
	   pred( pred ), 
	   subterms( subterms ),
	   subformulas( subformulas )
         { }

   };


   formula_node* repr;

public:
   formula( fol_oper op,
            const std::list< declaration > & variables,
	    const std::list< identifier > & pred,
	    const std::list< term > & subterms,
	    const std::list< formula > & subformulas )
   : repr( new formula_node::formula_node( op, 
                                           variables, pred,
				           subterms, subformulas ))
      {
         ( repr -> reference_count ) = 1;
      }


public:
   formula( const formula& other );
   void operator = ( formula other );
   ~formula( ); 


public: 
   static formula make_false( );
   static formula make_true( );
   static formula make_not( const formula& f );
   static formula make_and( const std::list< formula > & subforms );
   static formula make_or( const std::list< formula > & subforms );
   static formula make_and( const formula &f1, const formula &f2);
   static formula make_or( const formula &f1, const formula &f2);

   static formula make_implies( const formula& f1, const formula& f2 );
   static formula make_equiv( const formula& f1, const formula& f2 );

   static formula make_forall( const std::list< identifier > & variables, 
		               const formula& f );
   static formula make_exists( const std::list< identifier > & variables, 
		               const formula& f );
      // make_forall and make_exists construct first-order quantifications.
      // These are quantifications over 0-arity functions.

   static formula make_schematicforall( 
                                const std::list< declaration > & variables,
                                const formula& f );
   static formula make_schematicexists(
                                const std::list< declaration > & variables, 
                                const formula& f );
      // Note that schematic exists actually does not occur in our
      // system.

   static formula make_equals( const term& t1, const term& t2 );
   static formula make_atom( const identifier& p, 
		             const std::list< term > & subterms );
/*   static formula make_atom( const std::string& p, 
		             const std::list< term > & subterms );
*/

   fol_oper getoperator( ) const;
      
   std::list< declaration > :: const_iterator variables_begin( ) const; 
   std::list< declaration > :: const_iterator variables_end( ) const;
      // The declarations of quantified variables in case of 
      // fol_exists, fol_forall.

   std::list< term > :: const_iterator subterms_begin( ) const;
   std::list< term > :: const_iterator subterms_end( ) const;
      // The subterms in case of fol_equals, or fol_atom.

   std::list< formula > :: const_iterator subformulas_begin( ) const;
   std::list< formula > :: const_iterator subformulas_end( ) const;
      // The subformulas in case of fol_false, fol_true,
      // fol_not, fol_and, fol_or, fol_implies, fol_equiv,
      // fol_forall, fol_exists. 

   identifier getpredicate( ) const;
      // Gets the predicate in case of a non-equality atom. 

   friend std::ostream& operator << ( std::ostream& , const formula& f ); 

   friend bool operator== (const formula &a, const formula &b);

};


std::ostream& operator << ( std::ostream& , const formula& f ); 





#endif


