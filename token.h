#include "term.h"         
 #include "formula.h"      
 #include "substitution.h" 
 #include "values.h"       
 #include "identifier.h"   
 #include "context.h"      
 #include "prover.h"       


// This code was produced by Maphoon 2008.
// Definition of struct token:


#ifndef TOKEN_INCLUDED
#define TOKEN_INCLUDED    1


#include <list>
#include <iostream>



enum tokentype
{
   tkn__recover,
   tkn__defaultred,
   tkn_SCANERROR,
   tkn_EOF,
   tkn_IDENTIFIER,
   tkn_UNSIGNED,
   tkn_LPAR,
   tkn_RPAR,
   tkn_LSQBR,
   tkn_RSQBR,
   tkn_LANGBR,
   tkn_RANGBR,
   tkn_COMMA,
   tkn_COLON,
   tkn_COLONEQ,
   tkn_LAMBDA,
   tkn_MP,
   tkn_BY,
   tkn_INDIRECTLY,
   tkn_IN,
   tkn_END,
   tkn_AND,
   tkn_OR,
   tkn_IMPLIES,
   tkn_EQUIV,
   tkn_NOT,
   tkn_TRUE,
   tkn_FALSE,
   tkn_MINUS,
   tkn_PLUS,
   tkn_EQ,
   tkn_PROVE,
   tkn_FROM,
   tkn_ASSUME,
   tkn_DEFINE,
   tkn_EXISTENCE,
   tkn_UNIQUENESS,
   tkn_FUNCTION,
   tkn_PREDICATE,
   tkn_FORMULA,
   tkn_Proof,
   tkn_Prove,
   tkn_Permanent_Definition,
   tkn_Assumptions,
   tkn_Permanent_Assumptions,
   tkn_Subproof,
   tkn_Labeled_Formula,
   tkn_References,
   tkn_ASSUMERed,
   tkn_Declarations,
   tkn_Declaration,
   tkn_Function_Declaration,
   tkn_Predicate_Declaration,
   tkn_Formula_Declaration,
   tkn_Assume_Defined_Predicate,
   tkn_Assume_Defined_Function,
   tkn_Lambda_Bound_Vars,
   tkn_Labeled_Term,
   tkn_Define_Predicate,
   tkn_Define_Function_byPred,
   tkn_Define_Function_byTerm,
   tkn_Reference,
   tkn_Instantiations,
   tkn_Term,
   tkn_Formula,
   tkn_Ident_list,
   tkn_Bound_list,
   tkn_QForall,
   tkn_QExists,
   tkn_Atom,
   tkn_Term_list
};


struct token
{

   tokentype type;

   std::list< declaration > d;
   std::list< formula > f;
   std::list< identifier > id;
   std::list< identifier > idents;
   std::list< identifier > label;
   std::list< substitution > subst;
   std::list< term > t;
   std::list< unsigned int > uint;

   token( tokentype t )
      : type(t)
   {
   }

   token( );
      // Should have no definition.

   bool iswellformed( ) const;
      // Check if the attributes satisfy the
      // constraints.
};

std::ostream& operator << ( std::ostream& , const token& );



#endif


