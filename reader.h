
// Written by Hans de Nivelle & Piotr Witkowski, July 2008.

#ifndef READER_INCLUDED
#define READER_INCLUDED   1


#include <iostream>
#include <string>

//#include "../assert.h"
#include "token.h"


class reader
{
private:
   std::istream& stream;
   std::string name;

   char next;

   void moveforward( );


public: 
   
   unsigned int linenumber;
   unsigned int columnnumber;



   // The reader must have a public field lookahead:

   std::list< token > lookahead; 



   reader ( std::istream& istream = std::cin, 
            const std::string& name = "STDIN",
            unsigned int linenumber = 1,
            unsigned int columnnumber = 1 );

   // Create inputstream, set name, linenumber and columnumber.
   // Counting for the line- and columnnumber starts at 1. 
   // One must be somewhat careful that the stream (because it
   // cannot be copied) is stored as a reference. 

   ~reader( ); 
   // The next seen character, which was read already from the stream,
   // will be pushed back into the stream, unless it is EOF. 
   // The stream is not closed. 


   reader( const reader& );
   void operator = ( const reader& );
   // Have no definition.


   // The reader must have a method that reads a token and appends it
   // to lookahead. 
   // In case EOF is encountered, or some other problem with the input 
   // occurs, the reader should return some designated token for that,
   // for example EOF, READERROR or SCANERROR. 
 
   void scan( );


   // It must have a method of the following type: 

   void syntaxerror( ) const;
   // It is reasonable to put this function in the reader because
   // it needs to print something line 'line line L of file F'.


};


#endif


