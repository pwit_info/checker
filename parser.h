
// Code produced by Maphoon 2008.
// Declaration of parser:


#ifndef PARSER_INCLUDED
#define PARSER_INCLUDED     1


#include <list>
#include <iostream>


#include "token.h"
#include "reader.h"



void parser( reader& r,
      context & cont,
      prover & pr,
      unsigned int & bottom,
      tokentype start,
      unsigned int recoverlength );
         // In case of success, the parser returns a
         // start token, possibly followed by a 
         // lookahead token. Otherwise, it returns  
         // some other token. 




#endif


