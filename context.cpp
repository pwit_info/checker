

// Code written by Hans de Nivelle.


#include "context.h"


void context::assumefunction( identifier f, unsigned int arity, 
                              bool permanent )
{
   repr. push_back( property::property( ));

   repr. back( ). func. push_back( declaration::make_funcdecl( f, arity ));
   repr. back( ). permanent = permanent;
}


void context::definefunction( identifier f, unsigned int arity, 
                              bool permanent, const functionvalue& def )
{
   repr. push_back( property::property( ));

   repr. back( ). func. push_back( 
      funcassumption::funcassumption( 
         declaration::make_funcdecl( f, arity ), def ));
   repr. back( ). permanent = permanent;
}
                                   

void context::definefunction( identifier f, unsigned int arity,
                              bool permanent, 
                              const identifier& lab, const functionvalue& def )
{
   repr. push_back( property::property( ));

   repr. back( ). func. push_back(
      funcassumption::funcassumption(
         declaration::make_funcdecl( f, arity ), lab, def ));
   repr. back( ). permanent = permanent;
}


void context::definefunction( identifier f, unsigned int arity,
                              const predicatevalue& val, 
                              const std::string& proofexistence,
                              const std::string& proofuniqueness )
{
   repr. push_back( property::property( ));

   repr. back( ). funcpred. push_back(
      funcpreddefinition( 
         declaration::make_funcdecl( f, arity ), val, 
         proofexistence, proofuniqueness ));
   repr. back( ). permanent = true;
}


void context::definefunction( identifier f, unsigned int arity,
                              const identifier& lab,
                              const predicatevalue& val,
                              const std::string& proofexistence,
                              const std::string& proofuniqueness )
{
   repr. push_back( property::property( ));

   repr. back( ). funcpred. push_back(
      funcpreddefinition(
         declaration::make_funcdecl( f, arity ), lab, val,
         proofexistence, proofuniqueness ));
   repr. back( ). permanent = true;
}


// We move to predicates:


void context::assumepredicate( identifier f, unsigned int arity,
                               bool permanent )
{
   repr. push_back( property::property( ));

   repr. back( ). pred. push_back( declaration::make_preddecl( f, arity ));
   repr. back( ). permanent = permanent;
}


void context::definepredicate( identifier f, unsigned int arity,
                               bool permanent, const predicatevalue& def )
{
   repr. push_back( property::property( ));

   repr. back( ). pred. push_back(
      predassumption::predassumption(
         declaration::make_preddecl( f, arity ), def ));
   repr. back( ). permanent = permanent;
}


void context::definepredicate( identifier f, unsigned int arity,
                               bool permanent,
                               const identifier& lab, 
                               const predicatevalue& def )
{
   repr. push_back( property::property( ));

   repr. back( ). pred. push_back(
      predassumption::predassumption(
         declaration::make_preddecl( f, arity ), lab, def ));
   repr. back( ). permanent = permanent;
}


// Now come the formulas:


void context::assumeformula( formula f, bool permanent )
{
   repr. push_back( property::property( ));

   repr. back( ). form. push_back( formassumption::formassumption(f));
   repr. back( ). permanent = permanent;
}


void context::assumeformula( identifier lab, formula f, bool permanent )
{
   repr. push_back( property::property( ));

   repr. back( ). form. push_back( 
         formassumption::formassumption( lab, f ));
   repr. back( ). permanent = permanent;
}


void context::proveformula( formula f, bool permanent, 
                            const std::string& proof )
{
   repr. push_back( property::property( ));

   repr. back( ). form. push_back( 
      formassumption::formassumption( f, proof ));
   repr. back( ). permanent = permanent;
}


void context::proveformula( identifier lab, formula f, bool permanent,
                            const std::string& proof )
{
   repr. push_back( property::property( ));

   repr. back( ). form. push_back(
         formassumption::formassumption( lab, f, proof ));
   repr. back( ). permanent = permanent;
}


//////////////////////////////////////////////////////////////////
//                Methods of context::property
//////////////////////////////////////////////////////////////////



bool context::property::hasfunction( ) const
{
   return func. size( ) != 0 || funcpred. size( ) != 0;
}


bool context::property::haspredicate( ) const
{
   return pred. size( ) != 0;
}


bool context::property::hasformula( ) const
{
   if( func. size( ) != 0 && func. front( ). value. size( ) != 0 )
      return true;

   if( funcpred. size( ) != 0 )
      return true;

   if( pred. size( ) != 0 && pred. front( ). value. size( ) != 0 )
      return true;

   if( form. size( ) != 0 )
      return true;

   return false; 
}


bool context::property::haslabel( ) const
{
   if( func. size( ) != 0 && func. front( ). label. size( ) != 0 )
      return true;

   if( funcpred. size( ) != 0 && funcpred. front( ). label. size( ) != 0 )
      return true;

   if( pred. size( ) != 0 && pred. front( ). label. size( ) != 0 )
      return true;

   if( form. size( ) != 0 && form. front( ). label. size( ) != 0 )
      return true;

   return false;
}


declaration context::property::getdeclaration( ) const
{
   if( func. size( ) != 0 )
      return func. front( ). decl;

   if( funcpred. size( ) != 0 )
      return funcpred. front( ). decl;

   if( pred. size( ) != 0 )
      return pred. front( ). decl;

   ASSERT( false ); exit(0); 
}


formula context::property::getformula( ) const
{
   if( func. size( ) != 0 && func. front( ). value. size( ) != 0 )
   {
      // We construct equation of form 
      // all x1....xn f(x1,...,xn) = F(x1,...,xn):

      // Construct list of variables as terms:

      std::list< term > args;
      for( std::list< identifier > :: const_iterator
              v = func. front( ). value. front( ). boundvars. begin( );
              v != func. front( ). value. front( ). boundvars. end( );
              ++ v )
      {
         args. push_back( term::term( *v, std::list< term > :: list( )));
      }

      formula res = formula::make_equals(
                       term::term( func. front( ). decl. var, args ),
                       func. front( ). value. front( ). t );

      if( func. front( ). value. front( ). boundvars. size( ))
      {
         res = formula::make_forall( 
                     func. front( ). value. front( ). boundvars, res );
                        // We avoid adding an empty quantificaton.
      }
      return res;
   }

   if( funcpred. size( ))
   {
      // We construct an equivalence of form
      // all x1 ... xn y f(x1,...,xn) = y <-> P(x1,...xn,y).

      // Construct list of variables as terms:

      std::list< term > args;
      for( std::list< identifier > :: const_iterator
              v = funcpred. front( ). value. boundvars. begin( );
              v != funcpred. front( ). value. boundvars. end( );
              ++ v )
      {
         args. push_back( term::term( *v, std::list< term > :: list( )));
      }

      ASSERT( args. size( ));
      std::list< term > args_minus1 = args;
      args_minus1. pop_back( );

      formula res = formula::make_equals(
                 term::term( funcpred. front( ). decl. var, args_minus1 ),
                 args. back( ));

      res = formula::make_equiv( res, funcpred. front( ). value. f );
      if( funcpred. front( ). value. boundvars. size( ))
      {
         res = formula::make_forall( funcpred. front( ). value. boundvars, 
                                     res );
      }
      return res;
   }                   

   if( pred. size( ) && pred. front( ). value. size( ) != 0 )
   {
      // We construct an equivalence of form
      // all x1 ... xn p(x1,...,xn) <-> P(x1,...,xn). 

      // Construct list of variables as terms:

      std::list< term > args;
      for( std::list< identifier > :: const_iterator
              v = pred. front( ). value. front( ). boundvars. begin( );
              v != pred. front( ). value. front( ). boundvars. end( );
              ++ v )
      {
         args. push_back( term::term( *v, std::list< term > :: list( )));
      }

      formula res = formula::make_equiv(
                       formula::make_atom( pred. front( ). decl. var, args ),
                       pred. front( ). value. front( ). f );

      if( pred. front( ). value. front( ). boundvars. size( ))
      {
            res = formula::make_forall(
                            pred. front( ). value. front( ). boundvars, res );
      }
      return res;
   }

   if( form. size( ))
   {
      return form. front( ). f;
   } 

   ASSERT( false ); exit(0);  
}


identifier context::property::getlabel( ) const
{
   if( func. size( ) != 0 && func. front( ). label. size( ) != 0 )
      return func. front( ). label. front( ); 

   if( funcpred. size( ) != 0 && funcpred. front( ). label. size( ) != 0 )
      return funcpred. front( ). label. front( ); 

   if( pred. size( ) != 0 && pred. front( ). label. size( ) != 0 )
      return pred. front( ). label. front( ); 

   if( form. size( ) != 0 && form. front( ). label. size( ) != 0 )
      return form. front( ). label. front( ); 

   ASSERT( false ); exit(0); 
}

 
std::vector< context::property > :: const_iterator 
context::getfunctiondef( identifier id, unsigned int arity ) const
{
   if( repr. begin( ) != repr. end( ))
   {
      declaration decl = declaration::make_funcdecl( id, arity );
      std::vector< property > :: const_iterator p = repr. end( ); 
      do
      {
         -- p;
         if( p -> func. size( ) && p -> func. front( ). decl == decl )
            return p;

         if( p -> funcpred. size( ) && p -> funcpred. front( ). decl == decl )
            return p;
      }
      while( p != repr. begin( )); 
   }

   return repr. end( );
}


std::vector< context::property > :: const_iterator
context::getpredicatedef( identifier id, unsigned int arity ) const
{
   if( repr. begin( ) != repr. end( ))
   {
      declaration decl = declaration::make_preddecl( id, arity );
      std::vector< property > :: const_iterator p = repr. end( );
      do
      {
         -- p;
         if( p -> pred. size( ) && p -> pred. front( ). decl == decl )
            return p;
      }
      while( p != repr. begin( ));
   }

   return repr. end( );
}


std::vector< context::property > :: const_iterator
context::getformula( identifier lab, int offset ) const 
{
   std::vector< property > :: const_iterator p = getformula( lab );
   if( p != end( )) 
      p = findoffset( p, offset );
   return p;
}


std::vector< context::property > :: const_iterator
context::getformula( int offset ) const 
{
   std::vector< property > :: const_iterator p = end( );
   p = findoffset( p, offset );
   return p; 
}


std::vector< context::property > :: const_iterator
context::begin( ) const
{
   return repr. begin( );
}


std::vector< context::property > :: const_iterator
context::end( ) const 
{
   return repr. end( );
}


std::vector< context::property > :: const_iterator
   context::findcapture( std::vector< property > :: const_iterator f ) const
{
   ASSERT( f != end( ));

   formula form = f -> getformula( );

   ++ f; 
   while( f < end( ))
   {
      if( f -> func. size( ) && 
                  substitution::isfree( f -> func. front( ). decl, form ))
      {
         return f;
      }

      if( f -> funcpred. size( ) &&
                  substitution::isfree( f -> funcpred. front( ). decl, form ))
      {
         return f;
      }

      if( f -> pred. size( ) &&
                  substitution::isfree( f -> pred. front( ). decl, form ))
      {
         return f;
      }

      ++ f;
   }

   return end( ); 
}


unsigned int context::size( ) const 
{
   return repr. size( );
}


void context::changescope( unsigned int k, bool permanent)
{
   while( k < size( ) )
   {
      repr[k]. permanent = permanent;
      k++;
   }

}

void context::backtrack( unsigned int k ) 
throw( cannotabstractform, cannotabstractdecl )
{
   if( k >= size( ))
      return;
   else
   {
      backtrack( k + 1 ); 
         // This is a good, old-fashioned recursive procedure.

      if( ! repr [k]. permanent )
      {
         if( repr [k]. func. size( ))
         {
            // If we have a value, we substitute it away. Otherwise, 
            // we abstract. 

            if( repr [k]. func. front( ). value. size( ))
            {
               substitution subst;
               subst. assign_function( 
                         repr [k]. func. front( ). decl. var,
                         repr [k]. func. front( ). decl. arity,
                         repr [k]. func. front( ). value. front( ));

               substitute( repr. begin( ) + k + 1, subst );
            }
            else
            {
               abstract( repr. begin( ) + k + 1, 
                         repr [k]. func. front( ). decl );
            }
         }

         if( repr [k]. pred. size( ))
         {
            // If we have a value, we substitute it away. Otherwise,
            // we abstract.

            if( repr [k]. pred. front( ). value. size( ))
            {
               substitution subst;
               subst. assign_predicate(
                         repr [k]. pred. front( ). decl. var,
                         repr [k]. pred. front( ). decl. arity,
                         repr [k]. pred. front( ). value. front( ));

               substitute( repr. begin( ) + k + 1, subst );
            }
            else
            {
               abstract( repr. begin( ) + k + 1,
                         repr [k]. pred. front( ). decl );
            }
         }

         if( repr[k]. form. size( ))
         {
            abstract( repr. begin( ) + k + 1, 
                      repr [k]. form. front( ). f );
         }

         // And we do the actual deletion:

         for( unsigned int i = k + 1; i < size( ); ++ i )
            repr [ i - 1 ] = repr [i];
         repr. pop_back( );
      }
   }
}
 

bool context::contains( std::vector< property > :: const_iterator p,
                        declaration decl ) const
{
   while( p < end( ))
   {
      if( p -> func. size( ) && p -> func. front( ). decl == decl )
         return true;
      if( p -> funcpred. size( ) && p -> funcpred. front( ). decl == decl )
         return true;
      if( p -> pred. size( ) && p -> pred. front( ). decl == decl )
         return true;

      if( p -> hasformula( ) && 
                  substitution::contains( decl, p -> getformula( )))
         return true;
  
      ++ p;
   }
   return false;
}


void context::rename( std::vector< property > :: iterator p,
                      declaration decl, identifier val )
{
   while( p < end( ))
   {
      if( p -> func. size( ))
      {
         if( p -> func. front( ). decl == decl )
            p -> func. front( ). decl. var = val;

         if( p -> func. front( ). value. size( ))
         {
            p -> func. front( ). value. front( ). t =
               substitution::rename( decl, 
                                     p -> func. front( ). value. front( ). t,
                                     val );
         }
      }

      if( p -> funcpred. size( ))
      {
         if( p -> funcpred. front( ). decl == decl ) 
            p -> funcpred. front( ). decl. var = val;

         p -> funcpred. front( ). value. f =
                  substitution::rename( decl,
                                        p -> funcpred. front( ). value. f,
                                        val );
      }

      if( p -> pred. size( ))
      {
         if( p -> pred. front( ). decl == decl )
            p -> pred. front( ). decl. var = val;

         if( p -> pred. front( ). value. size( ))
         {
            p -> pred. front( ). value. front( ). f =
               substitution::rename( decl,
                                     p -> pred. front( ). value. front( ). f,
                                     val );
         }
      }

      if( p -> form. size( ))
      {
         p -> form. front( ). f = 
                 substitution::rename( decl, p -> form. front( ). f, val );
      }

      ++ p; 
   }
}


void context::substitute( std::vector< property > :: iterator p,
                          const substitution& subst )  
{

   while( p < end( ))
   {
      std::vector< property > :: iterator p1 = p; ++ p1;
         // This is the next position. We need it so often that it is
         // worth storing it.

      if( p -> func. size( ))
      {
         // If we have a value, we substitute in it.
         // The declared variable is not yet bound in the value,
         // so there is no need to check for conflicts. 

         if( p -> func. front( ). value. size( ))
         {
            p -> func. front( ). value. front( ) =
               subst. apply_on( p -> func. front( ). value. front( ));
         }

         if( subst. indomain( p -> func. front( ). decl ) ||
             subst. free_in_range( p -> func. front( ). decl ))
         {
            // Unfortunately, we need to invent a fresh variable.

            declaration newfunc = freshdecl( p -> func. front( ). decl,
                                             p1,
                                             subst ); 
            rename( p1, p -> func. front( ). decl, newfunc. var );
            p -> func. front( ). decl = newfunc;
         }
      }

      if( p -> funcpred. size( ))
      {
         // We substitute in the value. The defined variable is not
         // yet bound in the value.

         p -> funcpred. front( ). value =
                  subst. apply_on( p -> funcpred. front( ). value );

         if( subst. indomain( p -> funcpred. front( ). decl ) ||
             subst. free_in_range( p -> funcpred. front( ). decl ))
         {
            // We need to invent a fresh variable:

            declaration newfunc = freshdecl( p -> funcpred. front( ). decl,
                                             p1,
                                             subst );
            rename( p1, p -> funcpred. front( ). decl, newfunc. var );
            p -> funcpred. front( ). decl = newfunc;
         }
      }

      if( p -> pred. size( ))
      {
         // If we have a value, we substitute in it.
         // The declared variable is not yet bound in the value.

         if( p -> pred. front( ). value. size( ))
         {
            p -> pred. front( ). value. front( ) =
               subst. apply_on( p -> pred. front( ). value. front( ));
         }

         if( subst. indomain( p -> pred. front( ). decl ) ||
             subst. free_in_range( p -> pred. front( ). decl ))
         {
            // Unfortunately, we need to invent a fresh variable.

            declaration newpred = freshdecl( p -> pred. front( ). decl,
                                             p1,
                                             subst );
            rename( p1, p -> pred. front( ). decl, newpred. var );
            p -> pred. front( ). decl = newpred;
         }
      }

      if( p -> form. size( ))
      {
         p -> form. front( ). f = subst. apply_on( p -> form. front( ). f );
      }

      p = p1; 
   }
}


void context::abstract( std::vector< property > :: iterator p,
                        formula f ) throw( cannotabstractform )
{
   while( p < end( ))
   {
      if( p -> func. size( ) || p -> funcpred. size( ) || p -> pred. size( ))
         throw cannotabstractform::cannotabstractform( f, *p );

      ASSERT( p -> form. size( ) == 1 );

      // The fact that we don't abstract in definitions or declarations 
      // radically solves all capture problems.  

      p -> form. front( ). f = 
         formula::make_implies( f, p -> form. front( ). f );

      ++ p;
   }
}


// abstract a declaration. This is the most complicated function
// in this program. In case a 0-arity function is abstracted,
// it has to become a parameter of everything that is declared/defined
// afterwards.

void context::abstract( std::vector< property > :: iterator p,
                        declaration abstr_decl ) throw( cannotabstractdecl )
{
   while( p < end( ))
   {
      // std::cout << "abstracting " << abstr_decl << " in ";
      // std::cout << *p << "\n";

      std::vector< property > :: iterator p1 = p; ++ p1;
         // This is the next position. We need it so often that it is
         // worth storing it.

      if( p -> func. size( ) || p -> funcpred. size( ) || p -> pred. size( ))
      {
         // If abstr_decl is a predicate, or has arity != 0, we refuse.

         if( ! abstr_decl. isfunction || abstr_decl. arity != 0 )
            throw cannotabstractdecl::cannotabstractdecl( abstr_decl, *p );

         // We are going to pump up the arity of the declared variable by 
         // one.

         declaration* olddecl;
         if( p -> func. size( ))
            olddecl = & ( p -> func. front( ). decl );
         if( p -> funcpred. size( ))
            olddecl = & ( p -> funcpred. front( ). decl );
         if( p -> pred. size( ))
            olddecl = & ( p -> pred. front( ). decl );

         declaration newdecl = *olddecl;

         ++ newdecl. arity; 
         newdecl = freshdecl( newdecl, p, substitution::substitution( ));

         // We will substitute: 
         //   f_old := lambda x1 .... xn ( f_new abstr_decl x1 ... xn ).
         // As usual, it is a real challenge to produce the fresh 
         // variables x1, ..., xn.

         std::list< identifier > vars;
         vars. push_back( abstr_decl. var );
            // We will later remove abstr_decl again from the abstracted
            // variables. 

         for( unsigned int i = 0; i < olddecl -> arity; ++ i )
         {
            identifier id = 
                  freshdecl( 
                       declaration::make_funcdecl( std::string( "x" ), 0 ),
                       formula::make_forall( vars, 
                                             formula::make_false( ))). var;
            vars. push_back( id );
         }

         std::list< term > subterms;

         for( std::list< identifier > :: const_iterator
                 v = vars. begin( );
                 v != vars. end( );
                 ++ v )
         {
            subterms. push_back( 
                  term::term( *v, std::list< term > :: list( ))); 
         }

         vars. pop_front( );       // This removes abstr_decl. 

         substitution subst;
         if( olddecl -> isfunction )
         {
            subst. assign_function( 
                       olddecl -> var, olddecl -> arity, 
                       functionvalue::functionvalue( vars, 
                           term::term( newdecl. var, subterms ))); 
         }
         else
         {
            subst. assign_predicate( 
                       olddecl -> var, olddecl -> arity,
                       predicatevalue::predicatevalue( vars,
                           formula::make_atom( newdecl. var, subterms )));
         }

         substitute( p1, subst );

         // If we have a value, we need to abstract it.
         // We use hasformula( ) for checking if there is a value.

         if( p -> hasformula( ))
         {
            declaration added_par = abstr_decl; 

            if( ! substitution::isfree( added_par, p -> getformula( )))
            {
               // We run the risk that abs is among the bound variables 
               // of the value. Since in that case, it does not matter 
               // which variable we take, we produce a new variable.

               added_par = freshdecl( added_par, p -> getformula( ));
            } 

            if( p -> func. size( ))
               p -> func. front( ). value. front( ). boundvars. 
                                              push_front( added_par. var );
            if( p -> funcpred. size( ))
               p -> funcpred. front( ). value. boundvars. 
                                              push_front( added_par. var );
            if( p -> pred. size( ))
               p -> pred. front( ). value. front( ). boundvars. 
                                              push_front( added_par. var );
         }
     
         *olddecl = newdecl; 
            // And one must not forget to modify the declaration. 
      }

      if( p -> form. size( ))
      {
         declaration abstr_decl2 = abstr_decl;
         formula f = p -> form. front( ). f;

         if( ! substitution::isfree( abstr_decl2, f ))
            abstr_decl2 = freshdecl( abstr_decl2, f );

         std::list< declaration > vars;
         vars. push_back( abstr_decl2 );

         // If f already has form forall x1...xn, F, we prepend 
         // abstr_decl2 to the existing quantification:

         if( f. getoperator( ) == fol_forall )
         {
            for( std::list< declaration > :: const_iterator
                    v = f. variables_begin( );
                    v != f. variables_end( );
                    ++ v )
            {
               vars. push_back(*v);
            }

            f = * ( f. subformulas_begin( ));
         }

         p -> form. front( ). f = formula::make_schematicforall( vars, f );
      }
            
      p = p1;
   }
}


void context::checkdeclarations( formula f ) const
throw( undeclaredidentifier )
{
   std::list< declaration > localdeclarations;
   checkdeclarations( f, localdeclarations );
   ASSERT( localdeclarations. size( ) == 0 );
}


void context::checkdeclarations( const functionvalue& f ) const
throw( undeclaredidentifier ) 
{
   std::list< declaration > localdeclarations;
   for( std::list< identifier > :: const_iterator
           p = f. boundvars. begin( );
           p != f. boundvars. end( );
           ++ p )
   {
      localdeclarations. push_back( declaration::make_funcdecl( *p, 0 ));
   }

   checkdeclarations( formula::make_equals( f. t, f. t ), localdeclarations );

   ASSERT( localdeclarations. size( ) == f. getarity( ));
}


void context::checkdeclarations( const predicatevalue& val ) const
throw( undeclaredidentifier )
{
   std::list< declaration > localdeclarations;
   for( std::list< identifier > :: const_iterator
           p = val. boundvars. begin( );
           p != val. boundvars. end( );
           ++ p )
   {
      localdeclarations. push_back( declaration::make_funcdecl( *p, 0 ));
   }

   checkdeclarations( val. f, localdeclarations );

   ASSERT( localdeclarations. size( ) == val. getarity( ));
}


formula context::make_existence( const predicatevalue& val ) 
{

   // We construct a formula of form 
   //    all x1 ... xn exists y F(x1,...,xn,y).

   std::list< identifier > 
            vars = val. boundvars; 
   ASSERT( vars. size( ) != 0 ); 

   std::list< identifier > varslast;
   varslast. push_back( vars. back( ));
   vars. pop_back( );

   formula res = val. f;
   res = formula::make_exists( varslast, res );
 
   if( vars. size( ))
      res = formula::make_forall( vars, res ); 
   return res;
}


formula context::make_uniqueness( const predicatevalue& val ) 
{

   // We construct a formula of form
   //    all x1 ... xn all y y1 
   //       F(x1,...,xn,y) -> F(x1,...,xn,y1) -> y1 = y.

   std::list< identifier > vars = val. boundvars;
   ASSERT( vars. size( ) != 0 );

   // Our first problem is to find an appropriate y1. It must be fresh. 

   identifier y = vars. back( ); 
 
   formula testform = formula::make_forall( vars, val. f ); 
      // testform will not occur in the result. It is just a formula
      // containing all variables involved, so that we can use 
      // context::freshdecl for finding a fresh variable.

   identifier y1 = freshdecl( declaration::make_funcdecl( y, 0 ), 
                              testform ). var; 

   vars. push_back( y1 ); 

   // We build the final equality y1 = y.

   formula res = formula::make_equals( 
                    term::term( y1, std::list< term > :: list( )),
                    term::term( y, std::list< term > :: list( )));

   // Add F(x1,...,xn,y) [ y := y1 ]: 

   res = formula::make_implies(
             substitution::rename( declaration::make_funcdecl( y, 0 ),
                                   val. f, y1 ), 
             res );

   // Add F(x1,...,xn,y): 
   res = formula::make_implies( val. f, res );

   // Add the universal quantification: 

   ASSERT( vars. size( ) >= 2 ); 
   res = formula::make_forall( vars, res );

   return res;
}


std::ostream& operator << ( std::ostream& stream, 
                            const context::funcassumption& fa )
{
   stream << fa. decl;
   if( fa. value. size( ))
   {
      stream << "      := ";
      if( fa. label. size( ))
         stream << fa. label. front( ) << " : ";
      else
         stream << "           ";

      stream << fa. value. front( );
   }
   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const context::funcpreddefinition& fp )
{
   stream << fp. decl;
   stream << "      := ";
   if( fp. label. size( ))
      stream << fp. label. front( ) << " : ";
   else
      stream << "          ";

   stream << fp. value;
   stream << "           " << fp. proofexistence << " / " 
                           << fp. proofuniqueness; 

   return stream;
} 


std::ostream& operator << ( std::ostream& stream,
                            const context::predassumption& pa )
{
   stream << pa. decl;
   if( pa. value. size( ))
   {
      stream << "      := ";
      if( pa. label. size( ))
         stream << pa. label. front( ) << " : ";
      else
         stream << "           ";

      stream << pa. value. front( );
   }
   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const context::formassumption& fa )
{
   stream << "FORM ";

   if( fa. label. size( ))   
      stream << fa. label. front( ) << " : ";
   else
      stream << "       ";

   stream << fa. f;
   
   if( fa. proof. size( ))
      stream << "          " << fa. proof. front( );

   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const context::property& pr )
{
   ASSERT( pr. func. size( ) + pr. funcpred. size( ) + 
           pr. pred. size( ) + pr. form. size( ) == 1 );

   if( ! pr. permanent )
      stream << " (btrck) ";
   else
      stream << " (perm)  ";

   if( pr. func. size( ))
      stream << pr. func. front( );
   if( pr. funcpred. size( ))
      stream << pr. funcpred. front( );
   if( pr. pred. size( ))
      stream << pr. pred. front( );
   if( pr. form. size( ))
      stream << pr. form. front( );
  
   return stream;
}

 
std::ostream& operator << ( std::ostream& stream, const context& c )
{
   stream << "Context:\n";
   stream << "--------\n\n";

   for( std::vector< context::property > :: const_iterator
           p = c. repr. begin( );
           p != c. repr. end( );
           ++ p )
   {
      stream << *p << "\n";
   }
   stream << "\n";
   return stream;
}
   

std::ostream& operator << ( std::ostream& stream, 
                            const context::cannotabstractform& caf )
{ 
   stream << "cannot abstract: " << caf. f << "\n";
   stream << "into " << caf. prop << "\n";
   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const context::cannotabstractdecl& cad )
{
   stream << "cannot abstract: " << cad. d << "\n";
   stream << "into " << cad. prop << "\n";
   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const context::undeclaredidentifier& u )
{
   stream << u. d << " is used in " << u. f << " but not declared\n";
   return stream;
}


std::vector< context::property > :: const_iterator 
context::getformula( identifier lab ) const
{
   if( repr. begin( ) != repr. end( ))
   {
      // We look backwards, because it is the last one that counts.

      std::vector< property > :: const_iterator p = repr. end( );
      do
      {
         -- p;
         if( p -> haslabel( ) && p -> getlabel( ) == lab ) 
            return p;
      }
      while( p != repr. begin( ));
   }

   return repr. end( );
}


// It's hard to say what is the invariant here:
// At each time, p is either well-defined or end( ).
// On entry and exit, and each time offset is modified, 
// if p != end( ), then *p has a formula. 

std::vector< context::property > :: const_iterator
context::findoffset( std::vector< property > :: const_iterator p,
                     int offset ) const
{
   while( offset != 0 )
   {
      if( offset < 0 )
      {
         if( p == begin( ))
            return end( ); 

         -- p;
         if( p -> hasformula( ))
            ++ offset;
      }
      else
      {
         if( p == end( ))
            return p;

         ++ p;
         if( p == end( ))
            return p;

         if( p -> hasformula( ))
            -- offset;
      }
   }

   return p;
}
 

declaration context::freshdecl( declaration decl, formula f )
{
   unsigned int index = 0; 
   const std::string& s = decl. var. getstring( );
   decl. var = identifier::identifier( s, index );

   while( substitution::contains( decl, f ))
   {
      ++ index;
      decl. var = identifier::identifier( s, index );
   }

   return decl; 
}


declaration context::freshdecl( 
                         declaration decl, 
                         const std::vector< property > :: const_iterator p, 
                         const substitution& subst ) const 
{
   unsigned int index = 0;
   const std::string& s = decl. var. getstring( );
   decl. var = identifier::identifier( s, index );

   while( contains( p, decl ) ||
          subst. indomain( decl ) || subst. free_in_range( decl ))
   {
      ++ index;
      decl. var = identifier::identifier( s, index );
   }

   return decl;
}


namespace
{

   bool occursinlist( declaration decl,
                      const std::list< declaration > & decllist )  
   {
      for( std::list< declaration > :: const_iterator
              p = decllist. begin( );
              p != decllist. end( );
              ++ p )
      {
         if( decl == *p )
            return true;
      }
      return false;
   }

}
 

void context::checkdeclarations( 
                   formula f,
                   std::list< declaration > & localdeclarations ) const
throw( undeclaredidentifier )
{
   switch( f. getoperator( ))
   {
   case fol_false:
   case fol_true: 
      return; 

   case fol_not: 
   case fol_and:
   case fol_or:
   case fol_implies:
   case fol_equiv: 
      for( std::list< formula > :: const_iterator
              p = f. subformulas_begin( );
              p != f. subformulas_end( );
              ++ p )
      {
         checkdeclarations( *p, localdeclarations );
      }
      return;

   case fol_forall:
   case fol_exists:
      {
         unsigned int s = localdeclarations. size( ); 
         for( std::list< declaration > :: const_iterator
                 v = f. variables_begin( );
                 v != f. variables_end( );
                 ++ v )
         {
            localdeclarations. push_front( *v ); 
         }

         checkdeclarations( *( f. subformulas_begin( )), localdeclarations ); 

         while( localdeclarations. size( ) > s )
            localdeclarations. pop_front( );
      }
      return;

   case fol_equals:
   case fol_atom: 
      {
         unsigned int arity = 0;
         for( std::list< term > :: const_iterator 
                 p = f. subterms_begin( );
                 p != f. subterms_end( );
                 ++ p )  
         {
            checkdeclarations( *p, f, localdeclarations );
            ++ arity;
         }
    
         if( f. getoperator( ) == fol_atom )
         {
            declaration decl = 
                    declaration::make_preddecl( f. getpredicate( ), arity );
            if( getpredicatedef( decl. var, decl. arity ) != end( ) ||
                occursinlist( decl, localdeclarations )) 
            {
               return;
            }
            else
               throw undeclaredidentifier( decl, f ); 
         }
         else
            return; 
      }
   }

   ASSERT( false ); exit(0); 
}


void context::checkdeclarations(
                 term t,
                 formula f, 
                 const std::list< declaration > & localdeclarations ) const
throw( undeclaredidentifier )
{
   declaration decl = declaration::make_funcdecl( t. getfunction( ),
                                                  t. getnrsubterms( ));
   
   if( getfunctiondef( decl. var, decl. arity ) != end( ) ||
       occursinlist( decl, localdeclarations ))
   {
      for( std::list< term > :: const_iterator 
              p = t. subterms_begin( );
              p != t. subterms_end( );
              ++ p )
      {
         checkdeclarations( *p, f, localdeclarations );
      }
   }
   else
      throw undeclaredidentifier::undeclaredidentifier( decl, f );
}


 
