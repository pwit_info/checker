
#include "reader.h"


bool startidentifier( char c )
   {
      if( c >= 'a' && c <= 'z' ) return true;
      if( c >= 'A' && c <= 'Z' ) return true;
      if( c == '_' ) return true;
      return false;
   }

bool inidentifier( char c )
   {
      if( c >= 'a' && c <= 'z' ) return true;
      if( c >= 'A' && c <= 'Z' ) return true;
      if( c >= '0' && c <= '9' ) return true;
      if( c == '_' ) return true;
      return false;
   }

unsigned int make_unsigned( std::string::const_iterator p1, 
		               std::string::const_iterator p2 )

   {
      if( p1 == p2 )
	 {

        //throw error::error( "zero-length integer", mess );
           ASSERT(false);
         }
      ASSERT( p1 <= p2 );

      unsigned int val = 0;
      while( p1 != p2 )
      {
         char c = *p1;
	 if( ! isdigit(c) ) 
//            throw error::error( "non-digit in integer", mess );
	 ASSERT(false);
	 unsigned int x = c - '0';

	 ASSERT( x < 10 );

	 val = val * 10;
	 val += x;

	 ++ p1;
      }
      return val;
   }




void reader::moveforward( ) 
{
  if( stream && next != EOF )
  {
  
     if( next == '\t' )
        columnnumber += 5;         // An estimation. 
     else
     { 
        if( next == '\n' ) 
        {
           linenumber ++ ;
           columnnumber = 1;
        }
        else
           columnnumber ++ ;
     }

     next = stream. get( );
  }
}


 
reader::reader ( std::istream& stream,
                 const std::string& name, 
                 unsigned int linenumber,
                 unsigned int columnnumber ) :
   stream( stream ),
   linenumber( linenumber ), 
   columnnumber( columnnumber - 1 ),
   next( ' ' ),
   name( name ),
   lookahead( std::list< token >::list ( ))
   {  

   }


reader::~reader( ) 
   {
      if( next != EOF ) 
         stream. putback( next );
   }
 


void reader::scan( )
{

   whitespace:
      if( next == ' ' ||
          next == '\t' ||
          next == '\n' ||
	  next == 0x0D )
      {
         moveforward( );
	 goto whitespace;
      }

      if( next == EOF )
      {	 
         lookahead. push_back( tkn_EOF );
         return;
      }

      if( next == '(' )
      {
         moveforward( );
	 lookahead. push_back( tkn_LPAR );
	 return;
      }

      if( next == ')' )
      {
         moveforward( );
	 lookahead. push_back( tkn_RPAR );
	 return;
      }

      if( next == '[' )
      {
         moveforward( );
	 lookahead. push_back( tkn_LSQBR );
	 return;
      }

      if( next == ']' )
      {
         moveforward( );
	 lookahead. push_back( tkn_RSQBR );
	 return;
      }


      if( next == ',' )
      {
         moveforward( );
	 lookahead. push_back( tkn_COMMA );
	 return;
      }


      if( next == ':' )
      {
         moveforward( );
        
	 if( next == '=')
	 {
            moveforward( );
	    lookahead. push_back( tkn_COLONEQ );
	    return;
	 }
	 else
	 {
	    lookahead. push_back( tkn_COLON );   
	    return;
         }
      }

      if( next == '+' )
      {
         moveforward( );
	 lookahead. push_back( tkn_PLUS );
	 return;
      }


      if( next == '!' )
      {
         moveforward( );
	 lookahead. push_back( tkn_NOT );
	 return;
      }


      if( next == '/' )
      {
         // This could either
	 // an and:                     /\
	 // a slash:                    / */
	 // a comment:                  /*                 */
         // a comment:                  //          

         moveforward( );

	 if( next == '\\' )
         {
            moveforward( );
	    lookahead. push_back( tkn_AND );
	    return ;
         }

	 if( next == '/' )
         {
            moveforward( );
	    while( next != '\n' && next != EOF )
            {
               moveforward( );
            }

            if( next == EOF )
	    {
	       lookahead. push_back( tkn_SCANERROR );
               return;    // Because of EOF inside comment.
            }
            moveforward( );
	    goto whitespace;
         }

	 if( next == '*' )
         {
            moveforward( ); 

         incomment: 
	    while ( next != '*' && next != EOF )
            {
               moveforward( );
	    }

            if( next == EOF )
	    {
	       lookahead. push_back( tkn_SCANERROR );
               return;    // Because of EOF inside comment.
            }
	    moveforward( );

	    if( next == '/' )
            {
               moveforward( );
	       goto whitespace;   
            }

	    goto incomment; 
         }
         lookahead. push_back( tkn_SCANERROR );
	 return; 
      }

      if( next == '\\' )
      {
         moveforward( );
	 if( next == '/' )
         {
            moveforward( );
	    lookahead. push_back( tkn_OR);
	    return;
         }
	 lookahead. push_back( tkn_SCANERROR );
	 return;
      }

      if( next == '-' )
      {
         moveforward( );
	 if( next == '>' )
         {
            moveforward( );
	    lookahead. push_back( tkn_IMPLIES );
	    return;
         }

	 lookahead. push_back( tkn_MINUS );
         return;
      }

      if( next == '<' )
      {
         moveforward( );
	 if( next == '-' )
         {
            moveforward( );
	    if( next == '>' )
            {
               moveforward( );
	       lookahead. push_back( tkn_EQUIV );
	       return;
            }
	    lookahead. push_back( tkn_SCANERROR );
	    return;
         }

	 lookahead. push_back( tkn_LANGBR );
	 return;
      }

      if( next == '>' )
      {
         moveforward( );
	 lookahead. push_back( tkn_RANGBR );
	 return;
      }

      if( next == '=' )
      {
         moveforward( );
	 lookahead. push_back( tkn_EQ );
	 return;
      }


      if( startidentifier( next ))
      {
         std::string s;
         s. push_back( next );
	 moveforward( );

	 while( inidentifier( next ))
         {
            s. push_back( next );
	    moveforward( );
         }

	 if( s == std::string( "TRUE" ))
         {
            lookahead. push_back( tkn_TRUE );
	    return;
	 }

	 if( s == std::string( "FALSE" ))
	 {
	    lookahead. push_back( tkn_FALSE );
            return;
	 }

	 if( s == std::string( "FUNCTION" ))
	 {
	    lookahead. push_back( tkn_FUNCTION );
            return;
         }
	 if( s == std::string( "PREDICATE" ))
         {
	    lookahead. push_back( tkn_PREDICATE );
            return;
	 }

	 if( s == std::string( "PROVE" ))
	 {
	    lookahead. push_back( tkn_PROVE );
            return;
         } 

	 if( s == std::string( "FROM" ))
         {
	    lookahead. push_back( tkn_FROM );
	    return ;
         }

	 if( s == std::string( "DEFINE" ))
         {
	    lookahead. push_back( tkn_DEFINE );
	    return;
         }

	 if( s == std::string( "EXISTENCE" ))
         {
	    lookahead. push_back( tkn_EXISTENCE );
	    return;
         }

	 if( s == std::string( "UNIQUENESS" ))
         {
            lookahead. push_back( tkn_UNIQUENESS );
	    return;
         }

	 if( s == std::string( "FORMULA" ))
	 {
	    lookahead. push_back( tkn_FORMULA );
            return;
	 }

	 if( s == std::string( "MP" ))
	 {
	    lookahead. push_back( tkn_MP );
            return;
	 }

	 if( s == std::string( "ASSUME" ))
	 {
            lookahead. push_back( tkn_ASSUME );
            return;
         }

	 if( s == std::string( "IN" ))
	 {
	    lookahead. push_back( tkn_IN );
            return;
         }
	 
	 if( s == std::string( "END" ))
	 {
	    lookahead. push_back( tkn_END );
            return;

         }
	 if( s == std::string( "BY" ))
	 {
	    lookahead. push_back( tkn_BY );
            return;
         }

	 if( s == std::string( "INDIRECTLY" ))
	 {
	    lookahead. push_back( tkn_INDIRECTLY );
            return;
         }
/*	 
	 if( s == std::string( "AXIOM" ))
            return token::token_axiom;
*/
	 if( s == std::string( "LAMBDA" ))
	 {
	    lookahead. push_back( tkn_LAMBDA );
            return;
         }

   token t = tkn_IDENTIFIER;
   t. id. push_back( s);
   lookahead. push_back( t );
   return;
 }


      if( isdigit( next ))
      {
         std::string s;
	 s. push_back( next );
	 moveforward( );

	 while( isdigit( next ))
         {
            s. push_back( next );
	    moveforward( );
         }

	 token t = tkn_UNSIGNED;
	 t. uint. push_back( make_unsigned( s. begin( ), s. end( )) );
         lookahead. push_back( t );
	 return;
	 
      }



      // We could not read anything. 
      // We advance the tape by one, (in order to avoid non-termination)
      // and return the error token.

      moveforward( );
      lookahead. push_back( tkn_SCANERROR );
      
}


void reader::syntaxerror( ) const
{
   std::cout << "error in line " << linenumber << " of standard input\n";
   std::cout << "Unexpected: " << lookahead. front( )<< "\n";
}


