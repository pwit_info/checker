

// Code written by Hans de Nivelle, June 2006.


#ifndef TERM_INCLUDED
#define TERM_INCLUDED   1


#include "identifier.h"
#include "assert.h"


#include <list>
#include <vector>
//#include <string>

class term 
{

   struct term_node
   {
      unsigned int reference_count;
      identifier func; 
      std::list< term > subterms;
        

      term_node( identifier func, const std::list< term > & subterms )
         : func( func ),
	   subterms( subterms )
      {
      }


      term_node( identifier func, const std::vector< term > & subterms )
         : func( func ) 
      {
         for( std::vector< term > :: const_iterator 
		 p = subterms. begin( );
		 p != subterms. end( );
		 ++ p )
         {
            ( this -> subterms ). push_back( *p );
         }
      }  

   };


private:
   term_node* repr;

public: 
   term( const term& other );
   void operator = ( term other );
   ~term( );

   term( identifier f, const std::list< term > & subterms );
   term( identifier f, const std::vector< term > & subterms );
//   term( std::string f, const std::list< term > & subterms );
//   term( std::string f, const std::vector< term > & subterms );
   

   std::list< term > :: const_iterator subterms_begin( ) const;
   std::list< term > :: const_iterator subterms_end( ) const;

   identifier getfunction( ) const;
   unsigned int getnrsubterms( ) const;


   static int compare( const term &t1, const term &t2 );
      // -1 means: t1 is smaller.
      // 0 means: t1, t2 equal.
      // 1 means: t1 is bigger.
      // The order is not a well-order.

   bool operator < ( term other ) const;
   bool operator == ( term other ) const;
   bool operator != ( term other ) const; 

};

std::ostream& operator << ( std::ostream& stream, const term& t );


#endif


