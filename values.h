

#ifndef VALUES_INCLUDED
#define VALUES_INCLUDED    1


#include "identifier.h" 
#include "term.h"
#include "formula.h"



// A functionvalue can be viewed as 
// lambda boundvars t.

struct functionvalue 
{ 

   std::list< identifier > boundvars;
   term t;


   functionvalue( const std::list< identifier > & boundvars,
                  term t )
      : boundvars( boundvars ),
        t(t)
   {  } 
    

   functionvalue( term t )
      : t(t)
   { } 


   unsigned int getarity( ) const;

   bool iswellformed( ) const;
      // True if the bound variables are all distinct. 
    
   static functionvalue fromdecl( declaration d);
     // Given declaraton f:n creates f(x1,x2,...,xn) with boundvars = x1,...,xn
  
};

std::ostream& operator << ( std::ostream& , const functionvalue& );



// A predicatevalue can be viewed as
// lambda boundvars f.

struct predicatevalue
{

   std::list< identifier > boundvars;
   formula f;

   
   predicatevalue( const std::list< identifier > & boundvars,
                   formula f )
      : boundvars( boundvars ),
        f(f)
   {  }


   unsigned int getarity( ) const;

   bool iswellformed( ) const;
      // True if the bound variables are all distinct. 

   
   static predicatevalue fromdecl( declaration d);
     // Given declaration p:n creates p(x1,...xn) with boundvars = x1,...xn

};

std::ostream& operator << ( std::ostream& , const predicatevalue& ); 


#endif


