

// Code written by Hans de Nivelle, June 2006.



#include "formula.h"
#include <stdlib.h>

declaration declaration::make_funcdecl( identifier f, unsigned int arity )
{
   return declaration( f, arity, true );
}


declaration declaration::make_preddecl( identifier f, unsigned int arity )
{
   return declaration( f, arity, false );
}
/*
declaration declaration::make_funcdecl( std::string f, unsigned int arity )
{
   return declaration( identifier( f ), arity, true );
}

declaration declaration::make_preddecl( std::string f, unsigned int arity )
{
   return declaration( identifier( f ), arity, false );
}
*/
bool operator== ( const declaration &d1, const declaration &d2)
{

return  ( d1. var == d2. var) && ( d1.arity == d2. arity) &&
        ( d1. isfunction == d2. isfunction);

}

std::ostream& operator << ( std::ostream& stream,
                            const declaration& decl )
{
   if( decl. isfunction )
      stream << "FUNC ";
   else
      stream << "PRED ";

   stream << decl. var << " : " << decl. arity;
   return stream;
}



formula::formula( const formula& other )
   : repr( other. repr ) 
{
   ASSERT( repr -> reference_count ); 
   ++ ( repr -> reference_count );
}


// If you replace 'formula other' by 'const formula& other',
// the reference counting may mess up in case you assign
// a subterm to a superterm.

void formula::operator = ( formula other )
{
   -- ( repr -> reference_count );
   if( ( repr -> reference_count ) == 0 )
      delete repr;

   repr = other. repr;
   ASSERT( repr -> reference_count ); 
   ++ ( repr -> reference_count );
}

template < typename T>
bool equals( typename std::list< T> ::const_iterator  a_begin,
             typename std::list< T> :: const_iterator a_end,
             typename std::list< T> :: const_iterator b_begin, 
	     typename std::list< T> :: const_iterator b_end)
{

 while ( ( a_begin != a_end) && ( b_begin != b_end))
 {
    if( ! ( (*a_begin) == (*b_begin)))
       return false;
    a_begin ++; b_begin ++;
 }


 return true;
}


bool operator== (const formula &a, const formula &b)
{

   if ( a. getoperator( ) != b. getoperator( ))
       return false; 

 
   bool eqvars = equals< declaration> (
                              a. variables_begin( ), a. variables_end( ),
                              b. variables_begin( ), b. variables_end( ));
   bool eqterms = equals< term> ( 
                              a. subterms_begin( ), a. subterms_end( ),
                              b. subterms_begin( ), b. subterms_end( ));	  

   bool eqformulas = equals< formula> ( 
                              a. subformulas_begin( ), a. subformulas_end( ),
                              b. subformulas_begin( ), b. subformulas_end( ));
           
   bool eqpredicates = ( a. getoperator( ) == fol_atom ) ?
                                 ( a. getpredicate( ) == b. getpredicate( ) ) 
				 : 
				 true;

   return eqvars && eqterms && eqformulas && eqpredicates;
}

formula::~formula( )
{
   -- ( repr -> reference_count );
   if( ( repr -> reference_count ) == 0 )
      delete repr;
}



formula formula::make_false( ) 
{
   return formula::formula( fol_false,
		      std::list< declaration > :: list( ),
		      std::list< identifier > :: list( ),
		      std::list< term > :: list( ),
		      std::list< formula > :: list( ));
}


formula formula::make_true( ) 
{
   return formula::formula( fol_true,
		      std::list< declaration > :: list( ),
		      std::list< identifier > :: list( ),
		      std::list< term > :: list( ),
		      std::list< formula > :: list( ));
}


formula formula::make_not( const formula& f )
{
   std::list< formula > sub;
   sub. push_back(f); 
   return formula::formula( fol_not,
		      std::list< declaration > :: list( ),
		      std::list< identifier > :: list( ),
		      std::list< term > :: list( ),
		      sub );
}


formula formula::make_and( const std::list< formula > & subforms ) 
{
   return formula::formula( fol_and,
		               std::list< declaration > :: list( ),
			       std::list< identifier > :: list( ),
			       std::list< term > :: list( ),
			       subforms );
}


formula formula::make_or( const std::list< formula > & subforms )
{
   return formula::formula( fol_or,
		               std::list< declaration > :: list( ),
		               std::list< identifier > :: list( ),
		               std::list< term > :: list( ),
		               subforms );
}

formula formula::make_and( const formula &f1, const formula &f2)
{
   std::list< formula > subforms;

   subforms. push_back( f1 );
   subforms. push_back( f2 );

   return make_and( subforms );
}

formula formula::make_or( const formula &f1, const formula &f2)
{
   std::list< formula > subforms;

   subforms. push_back( f1 );
   subforms. push_back( f2 );

   return make_or( subforms );
}



formula formula::make_implies( const formula& f1, const formula& f2 )
{
   std::list< formula > sub;
   sub. push_back(f1);
   sub. push_back(f2); 
   return formula::formula( fol_implies,
		               std::list< declaration > :: list( ),
			       std::list< identifier > :: list( ),
			       std::list< term > :: list( ),
			       sub );
}


formula formula::make_equiv( const formula& f1, const formula& f2 )
{
   std::list< formula > sub;
   sub. push_back(f1);
   sub. push_back(f2);
   return formula::formula( fol_equiv,
                            std::list< declaration > :: list( ),
			    std::list< identifier > :: list( ),
			    std::list< term > :: list( ),
			    sub );
}


formula formula::make_forall( const std::list< identifier > & variables,
		              const formula& f )
{
   std::list< declaration > decls;
   for( std::list< identifier > :: const_iterator 
           p = variables. begin( ); 
           p != variables. end( ); 
           ++ p )
   {
      decls. push_back( declaration::make_funcdecl( *p, 0 ));
   }

   std::list< formula > sub;
   sub. push_back(f);

   return formula::formula( fol_forall,
		               decls,
			       std::list< identifier > :: list( ),
			       std::list< term > :: list( ),
			       sub );
}


formula formula::make_exists( const std::list< identifier > & variables,
		              const formula& f )
{
   std::list< declaration > decls;
   for( std::list< identifier > :: const_iterator
           p = variables. begin( ); 
           p != variables. end( ); 
           ++ p )
   {
      decls. push_back( declaration::make_funcdecl( *p, 0 ));
   }
 
   std::list< formula > sub;
   sub. push_back(f);

   return formula::formula( fol_exists,
                               decls,
			       std::list< identifier > :: list( ),
			       std::list< term > :: list( ),
			       sub );
}


formula formula::make_schematicforall( 
                              const std::list< declaration > & variables,
                              const formula& f )
{
   std::list< formula > sub;
   sub. push_back(f);

   return formula::formula( fol_forall,
                               variables,
                               std::list< identifier > :: list( ),
                               std::list< term > :: list( ),
                               sub );
}


formula formula::make_schematicexists( 
                              const std::list< declaration > & variables,
                              const formula& f )
{
   std::list< formula > sub;
   sub. push_back(f);

   return formula::formula( fol_exists,
                               variables,
                               std::list< identifier > :: list( ),
                               std::list< term > :: list( ),
                               sub );
}


formula formula::make_equals( const term& t1, const term& t2 )
{
   std::list< term > sub;
   sub. push_back(t1);
   sub. push_back(t2);

   return formula::formula( fol_equals,
		               std::list< declaration > :: list( ),
			       std::list< identifier > :: list( ),
			       sub, 
			       std::list< formula > :: list( ));
}


formula formula::make_atom( const identifier& p,
		            const std::list< term > & subterms )
{
   std::list< identifier > pred;
   pred. push_back(p);

   return formula::formula( fol_atom, 
		               std::list< declaration > :: list( ),
			       pred, 
			       subterms,
			       std::list< formula > :: list( ));
}
/*
formula formula::make_atom( const std::string& p,
		            const std::list< term > & subterms )
{
   std::list< identifier > pred;
   pred. push_back( identifier( p ) );

   return formula::formula( fol_atom, 
		               std::list< declaration > :: list( ),
			       pred, 
			       subterms,
			       std::list< formula > :: list( ));
}



*/

   
fol_oper formula::getoperator( ) const
{
   return ( repr -> op );
}


std::list< declaration > :: const_iterator 
formula::variables_begin( ) const
{
//   ASSERT( ( repr -> op ) == fol_forall || ( repr -> op ) == fol_exists );
   return ( repr ->  variables ). begin( );
}


std::list< declaration > :: const_iterator
formula::variables_end( ) const
{
//   ASSERT( ( repr -> op ) == fol_forall || ( repr -> op ) == fol_exists );
   return ( repr -> variables ). end( );
}


std::list< term > :: const_iterator 
formula::subterms_begin( ) const
{
//   ASSERT( ( repr -> op ) == fol_equals || ( repr -> op ) == fol_atom );
   return ( repr -> subterms ). begin( );
}

std::list< term > :: const_iterator
formula::subterms_end( ) const
{
//   ASSERT( ( repr -> op ) == fol_equals || ( repr -> op ) == fol_atom );
   return ( repr -> subterms ). end( );
}


std::list< formula > :: const_iterator
formula::subformulas_begin( ) const
{
   fol_oper op = ( repr -> op );
//   ASSERT( op == fol_false || op == fol_true ||
//           op == fol_not ||
//	   op == fol_and || op == fol_or ||
//	   op == fol_implies || op == fol_equiv ||
//	   op == fol_forall || op == fol_exists );
   return ( repr -> subformulas ). begin( );
}


std::list< formula > :: const_iterator
formula::subformulas_end( ) const
{
   fol_oper op = ( repr -> op );
//   ASSERT( op == fol_false || op == fol_true ||
//           op == fol_not ||
//           op == fol_and || op == fol_or ||
//           op == fol_implies || op == fol_equiv ||
//           op == fol_forall || op == fol_exists );
   return ( repr -> subformulas ). end( );
}


identifier formula::getpredicate( ) const
{
   ASSERT( ( repr -> op ) == fol_atom );
   return ( repr -> pred ). front( ); 
}


// We always print in such a way that the result is immune againts
// every context. Because of this, we don't need to
// bother about subformulas. 

std::ostream& operator << ( std::ostream& stream, const formula& f )
{
   switch( f. repr -> op )
   {
   case fol_false:
      stream << "FALSE";
      return stream;

   case fol_true:
      stream << "TRUE";
      return stream;
      
   case fol_not:
      stream << "! ";
      stream << * f. subformulas_begin( );
      return stream; 

   case fol_and:
   case fol_or:
   case fol_implies:
   case fol_equiv: 
      stream << "( ";

      for( std::list< formula > :: const_iterator 
	      s = f. subformulas_begin( ); 
	      s != f. subformulas_end( ); 
	      ++ s )
      {
         if( s != f. subformulas_begin( ))
         {
            switch( f. repr -> op )
            {
            case fol_and:      stream << " /\\ "; break;
            case fol_or:       stream << " \\/ "; break;
            case fol_implies:  stream << " -> ";  break;
            case fol_equiv:    stream << " <-> "; break;
	    default: ASSERT(false);
            }
         }

	 stream << *s;
      }
      stream << " )"; 
      return stream;

   case fol_exists:
   case fol_forall: 
      if( ( f. repr -> op ) == fol_exists )
         stream << "( <";
      if( ( f. repr -> op ) == fol_forall )
         stream << "( [";

      for( std::list< declaration > :: const_iterator 
	      v = f. variables_begin( );
	      v != f. variables_end( );
	      ++ v )
      {
         if( v != f. variables_begin( ))
            stream << ",";
	 stream << " " << *v;
      }

      if( ( f. repr -> op ) == fol_exists )
         stream << " > ";
      if( ( f. repr -> op ) == fol_forall )
         stream << " ] ";

      stream << *f. subformulas_begin( );
      stream << " )";
      return stream; 

   case fol_equals:
      {
         std::list< term > :: const_iterator t = f. subterms_begin( );
	 stream << *t;
	 stream << " = ";
	 ++ t;
	 stream << *t;
      }
      return stream;

   case fol_atom: 
      stream << f. getpredicate( );
      if( f. subterms_begin( ) != f. subterms_end( ))
      {
         stream << "(";
         for( std::list< term > :: const_iterator 
                 t = f. subterms_begin( );
		 t != f. subterms_end( );
		 ++ t )
         {
            if( t != f. subterms_begin( ))
               stream << ",";
	    stream << " " << *t;
         }
	 stream << " )";
      }
      return stream;
   }

   ASSERT(false); exit(0); 
}




