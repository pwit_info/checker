

// Code written by Hans de Nivelle, May 2008.


#ifndef PROVER_INCLUDED
#define PROVER_INCLUDED    1


#include <string>
#include <iostream>


#include "provergoal.h"


struct prover
{


   // Readerror means that either the file could not be opened, or we 
   // could not read from it.

   struct readerror
   {
      std::string f;

      readerror( const std::string& f )
         :f(f)
      {
      }

   };


   // Writeerror means that either the file could not be opened,
   // or we could not write into it.

   struct writeerror
   {
      std::string f;

      writeerror( const std::string& f )
         :f(f)
      {
      }

   };


   prover( ); 


   std::string name;
      // For example Geo, Spass, Otter, E. This string is just like
      // a comment. Nothing is done with it. 

   std::string inputtemplate;
      // Template according to which the names of the input files
      // are created. For example "/home/nivelle/atp/inp"
      // If usecounter == true, the inputs will be in files 
      //       /home/nivelle/atp/inp0000,
      //       /home/nivelle/atp/inp0001,
      //       /home/nivelle/atp/inp0002.
      // If usefilecounter == false, the inputs will all be in a single file
      // that is reused after each call:  /home/nivelle/inputs/inp

   std::string outputtemplate;
      // Same as for the outputs. For example 
      //      /home/nivelle/atp/outp. 
      // I think it's a good idea when inputtemplate != outputtemplate.

   std::string command;
      // Command with which the theorem prover should be called, e.g. 
      // home/nivelle/programs/geo2007f/geo 
      // -nonempty -tptp-input -inputfile %i > %o
      // The following parameters can occur in the string:
      //
      //     %i : name of input file.
      //     %t : time out. (if the prover can handle this)  
      //     %o : name of output file. 
      //     %n : replaced by newline \n. (so that multiple commands can be 
      //             given in one string, e.g. when using tptp2X) 
  
 
   std::string acceptingstring;
      // The string with which proofs can be recognized. 
      // For example "END-OF-PROOF"

   unsigned int filecounter; 
   bool usefilecounter;
      // If this flag is set, different inputs are put in different files.

   bool runprover;
      // If this flag is set to false, the prover is not run, and every
      // goal is considered proven. 
      // This option is intended for a situation, where you want to generate
      // the inputs to the prover, without running the prover. 
      // (If you want to pass them to somebody who wants to try them on
      //  another prover) 

   std::string timeout; 
      // This string is passed to the prover without change. 
      // (If %t occurs in the command)

   bool trytoprove( const provergoal& pg ) 
      throw( readerror, writeerror, notfirstorder );
      // Go through the whole procedure: Generate the filenames,
      // open the input file, call pg. print_tptp( <the inputfile> ). 
      // call the prover, and check the
      // result for the acceptingstring. Return true if everything
      // went well and the acceptingstring was found. 


   static prover readprover( const std::string& filename ) throw( readerror );
      // Read the prover information from filename.
      
private:

   static std::string createcommand( const std::string& command,
                                     const std::string& inputfile,
                                     const std::string& outputfile,
                                     const std::string& timeout );


   bool containsacceptingstring( const std::string& filename ) const
   throw( readerror ); 
      // Returns true if filename contains the accepting string. 
};


std::ostream& operator << ( std::ostream& stream, const prover& );
std::ostream& operator << ( std::ostream& stream, const prover::readerror& );
std::ostream& operator << ( std::ostream& stream, const prover::writeerror& );


#endif


