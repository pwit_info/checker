

// Code written by Hans de Nivelle, May 2008.


#include "tptp_namemap.h"



const std::string& tptp_namemap::lookup( const declaration& decl )
{
   
   // First look in local variables:

   for( std::list< boundvar > :: const_iterator
           p = local. begin( );
           p != local. end( );
           ++ p )
   {
      if( p -> decl == decl )
         return p -> val;
   }


   // Then look in global variables:

   std::map< declaration, std::string, 
             declaration_comparator > :: const_iterator 
      it = global. find( decl );

   if( it != global. end( ))
      return it -> second;

   // Now we will have to invent a fresh name:

   const std::string& fresh = 
      makefreshstring( decl. var. getstring( ), false );

   global. insert( std::pair< declaration, std::string > :: pair(
                                    decl, fresh ));

   return fresh; 
}


const std::string& tptp_namemap::declarelocal( const declaration& decl )
{
   const std::string& fresh = makefreshstring( decl. var. getstring( ), 
                                               true );
   local. push_front( boundvar::boundvar( decl, fresh ));
   return fresh;
}


unsigned int tptp_namemap::nrlocaldeclarations( ) const
{
   return local. size( );
}


void tptp_namemap::backtrack( unsigned int k )
{
   ASSERT( k <= local. size( ));

   while( k < local. size( ))
   {
      // usedstrings. erase( local. front( ). val ); 
      local. pop_front( );
   }
}



namespace
{

   bool isletter( char c )
   {
      return ( c >= 'a' && c <= 'z' ) ||
             ( c >= 'A' && c <= 'Z' );
   }


   bool isdigit( char c )
   { 
      return ( c >= '0' && c <= '9' );
   }

   
   bool isuppercase( char c )
   {
      return ( c >= 'A' && c <= 'Z' );
   }


   bool islowercase( char c )
   {
      return ( c >= 'a' && c <= 'z' );
   }
        

   char makelowercase( char c )
   {
      return c - 'A' + 'a';
   }


   char makeuppercase( char c )
   {
      return c - 'a' + 'A';
   }


   // We try to replace special characters by meaningful strings.

   std::string replacespecial( char c )
   {
      switch(c)
      {
      case '~':
         return "tilde";
      case '!':
         return "exclamationmark";
      case '@':
         return "at";
      case '#':
         return "hash";
      case '$':
         return "dollar"; 
      case '%':
         return "percent";
      case '^':
         return "hat";
      case '&':
         return "ampersand";
      case '*':
        return "star";
      case '(':
        return "lpar";
      case ')':
        return "rpar";
      case '-':
        return "minus";   
      case '+':
        return "plus";   
      case '=':
        return "equals";
      case '|':
        return "bar";
      case '\\':
         return "backslash";
      case '{':
         return "leftaccolade";
      case '}':
         return "rightaccolade";
      case '[':
         return "lsqpar";
      case ']':
         return "rsqpar";
      case ':':
         return "colon";
      case ';':
         return "semicolon";
      case '<':
         return "lessthan";
      case '>':
         return "greaterthan";
      case '?':
         return "questionmark";
      case '/':
         return "slash";
      case ' ':
         return "_"; 
      default:
         return "spec"; 
      }
   } 


}


// Make a legal TPTP-string out of s. The transformation is not injective,
// but that is no problem, since later an index will be added, which will
// ensures uniqueness.


std::string tptp_namemap::legalize( const std::string& s, bool uppercase )
{

   std::string res;

   // 1: We copy the string, and replace special symbols. 

   for( unsigned int kk = 0; kk < s. size( ); ++ kk )
   {
      char c = s [kk];

      if( ! isletter(c) && ! isdigit(c) && c != '_' )
         res += replacespecial(c);  
      else
         res. push_back(c);  
   }

   // 2: If uppercase, we make sure that all letters in the string are 
   //    uppercase.
   //    If ! uppercase, we make sure that all letters in the string are 
   //    lowercase. 

   if( uppercase )
   {
      for( std::string::iterator 
              p = res. begin( );
              p != res. end( );
              ++ p ) 
      {
         if( islowercase( *p ))
            *p = makeuppercase( *p );
      }
   }
   else
   {
      for( std::string::iterator
              p = res. begin( );
              p != res. end( );
              ++ p )
      {
         if( isuppercase( *p ))
            *p = makelowercase( *p );
      }
   }

   // 3: If uppercase, we ensure that the string starts with a capital letter.
   //    If ! uppercase, we ensure that the string starts with a 
   //    non-capital letter:

   if( uppercase && ( res. size( ) == 0 || ! isuppercase( res[0] )))  
      return 'X' + res; 
   if( ! uppercase && ( res. size( ) == 0 || ! islowercase( res[0] )))
      return 'x' + res;
   return res;  
}



namespace
{

   // Returns something of form sXXXX, where XXXX represents i.

   std::string addindex( std::string s, unsigned int i )
   {
      unsigned int dig = 1;
      while( dig <= i )
         dig *= 10;

      dig = dig / 10;

      while( dig != 0 )
      {
         unsigned int d = i / dig;
         i = i % dig;

         s. push_back( '0' + d );
         dig = dig / 10;
      }

      return s;
   }

}


const std::string& tptp_namemap::makefreshstring( const std::string& s,
                                                  bool uppercase )
{
   std::string base = legalize( s, uppercase );

   unsigned int ind = 0;
   std::string fresh = addindex( base, ind );

   while( usedstrings. find( fresh ) != usedstrings. end( ))
   {
      ++ ind;
      fresh = addindex( base, ind );
   }


   return usedstrings. insert( std::pair< std::string, bool > :: pair(
                                      fresh, true )). first -> first;
      // Insert fresh into usedstrings, return the reference to the 
      // inserted string. Returning a reference to fresh would be dangerous. 
}


std::ostream& operator << ( std::ostream& stream, const tptp_namemap& map )
{
   stream << "TPTP namemap:\n";

   stream << "global name mapping:\n";
   for( std::map< declaration, std::string, 
                  tptp_namemap::declaration_comparator > :: const_iterator 
           p = map. global. begin( );
           p != map. global. end( );
           ++ p )
   {
      stream << "   " << ( p -> first ) << " => " << ( p -> second ) << "\n";
   }

   stream << "local name mapping (most recent first):\n";

   for( std::list< tptp_namemap::boundvar > :: const_iterator 
           p = map. local. begin( );
           p != map. local. end( );
           ++ p )
   {
      stream << "   " << p -> decl << " => " << ( p -> val ) << "\n";
   }

   stream << "used names:\n"; 
   stream << "   { "; 
   for( std::map< std::string, bool > :: const_iterator
           p = map. usedstrings. begin( );
           p != map. usedstrings. end( );
           ++ p )
   {
      if( p != map. usedstrings. begin( ))
         stream << ", ";
      stream << ( p -> first );
   }
   stream << " }\n";

   return stream; 
}


