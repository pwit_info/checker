

// Code written by Hans de Nivelle, 2008.
//
// This is the most complicated class in this system, because
// it contains the complete reasoning core of the system.


#ifndef CONTEXT_INCLUDED
#define CONTEXT_INCLUDED   1


#include <map> 
#include <list> 
#include <vector>
#include <stdlib.h>

#include "formula.h"
#include "substitution.h" 


class context
{

public: 
   // A funcassumption assumes a function symbol, and possibly
   // assigns a value to it.

   struct funcassumption
   {
      declaration decl;

      std::list< functionvalue > value;
         // In case a value is given, we are a definition. 

      std::list< identifier > label;
         // In case a value is given, it is possible to give a label
         // to the characteristic formula.

      funcassumption( const declaration& decl )
         : decl( decl )
      {
         ASSERT( decl. isfunction );
      }


      funcassumption( const declaration& decl, const functionvalue& val )
         : decl( decl ) 
      {
         ASSERT( decl. isfunction );
         ASSERT( decl. arity == val. getarity( ));
         value. push_back( val ); 
      }

 
      funcassumption( const declaration& decl, identifier lab, 
                                               const functionvalue& val )
         : decl( decl ) 
      {
         ASSERT( decl. isfunction );
         ASSERT( decl. arity == val. getarity( ));
         value. push_back( val );
         label. push_back( lab );
      }

   };


   // A funcpredassumption introduces a function symbol, and defines
   // it by a characteristic predicate. I don't like it when symmetry is
   // broken, but in this case it makes no sense having two options,
   // (declaration and definition). 

   struct funcpreddefinition
   {
      declaration decl;

      predicatevalue value;
         // If decl has arity n, then value must have arity n + 1.
         // If value equals [x1,...xn,y] F(x1,...,xn,y), then the
         // defining formula equals 
         //          all x1...xn y f(x1,...,xn) = y <-> F(x1,...,xn,y). 

      std::list< identifier > label;


      std::string proofexistence;
      std::string proofuniqueness; 


      funcpreddefinition( const declaration& decl,
                          const predicatevalue& val,
                          const std::string& prfext,
                          const std::string& prfuni )
         : decl( decl ),
           value( val ),
           proofexistence( prfext ),
           proofuniqueness( prfuni )
      {
         ASSERT( decl. isfunction );
         ASSERT( decl. arity + 1 == val. getarity( )); 
      }    
                           
      funcpreddefinition( const declaration& decl,
                          identifier lab, 
                          const predicatevalue& val,
                          const std::string& prfext,
                          const std::string& prfuni )
         : decl( decl ),
           value( val ),
           proofexistence( prfext ),
           proofuniqueness( prfuni )
      {
         ASSERT( decl. isfunction );
         ASSERT( decl. arity + 1 == val. getarity( ));

         label. push_back( lab );
      } 
          
   };

 
   // A predassumption assumes a predicate symbol, and possibly
   // gives a value for it.
 
   struct predassumption
   {
      declaration decl;

      std::list< predicatevalue > value;
         // In case a value is given, we are a definition.

      std::list< identifier > label;
         // In case a value is given, it is possible to give a label
         // to the characteristic formula.

      predassumption( const declaration& decl )
         : decl( decl )
      { 
         ASSERT( ! decl. isfunction );
      }


      predassumption( const declaration& decl, const predicatevalue& val )
         : decl( decl )
      {
         ASSERT( ! decl. isfunction );
         ASSERT( decl. arity == val. getarity( ));
         value. push_back( val );
      }


      predassumption( const declaration& decl, identifier lab, 
                      const predicatevalue& val )
         : decl( decl )
      {
         ASSERT( ! decl. isfunction );
         ASSERT( decl. arity == val. getarity( ));
         value. push_back( val );
         label. push_back( lab );
      }

   }; 

  
   // A formula assumes a formula, possibly attaching a name to it.
   // It is also possible to give a proof for the formula. We go not
   // really check the proof, but one could add a phrase like 
   // 'proven by Geo, proven by Spass, proven by Otter' etc. 


   struct formassumption
   {
      std::list< identifier > label;
      formula f;

      std::list< std::string > proof;
         // This is a joke of course, since we have no way of checking proofs.

      formassumption( formula f )
         : f(f)
      {  }

      formassumption( identifier label, formula f )
         : f(f)
      {
         this -> label. push_back( label );
      } 

      formassumption( formula f, const std::string& proof )
         : f(f)
      {
         this -> proof. push_back( proof );
      }

      formassumption( identifier label, formula f, const std::string& proof )
         : f(f) 
      {
         this -> label. push_back( label );
         this -> proof. push_back( proof );
      }
   
   }; 
 

   struct property 
   {
      std::list< funcassumption > func; 
      std::list< funcpreddefinition > funcpred; 
      std::list< predassumption > pred;
      std::list< formassumption > form;
         // Exactly one of these has to be present.
         // 
         // Each of funcassumption, predassumption, formassumption,
         // can either be a definition or a declaration.
         // funcpreddefinition can only be a definition.
         //
         // Each of funcassumption, predassumption, formassumption can be 
         // either backtrackable or non-backtrable. funcpreddefinition
         // cannot be backtrackable.
         //
         // The reasons for these ugly assymmetries are as follows:
         // In case funcpreddefinition contains no definition, one should
         // use funcasssumption.
         // In case a value is given, the definition is removed by 
         // substitution. We have no way of substituting away a function
         // that is defined by a predicate.

      bool permanent;
         // What is permanent, cannot be removed.
 
      bool hasfunction( ) const;
         // True if we introduce a function. This means that we have
         // either a funcassumption or a funcpreddefinition.

      bool haspredicate( ) const;
         // True if we introduce a predicate. This means that we have
         // a predassumption.

      bool hasformula( ) const; 
      bool haslabel( ) const; 
         // True if we have a formula. This is always the case when we 
         // are defining something, and of course also when we assume
         // or prove a formula. A formula may have or not have a label. 

      declaration getdeclaration( ) const; 
         // Either a function or predicate declaration.

      formula getformula( ) const;
         // It is essential, (for correct capture avoidance) that the   
         // formula contains all variables of the property.
         // It may contain more, but it should not contain less.

      identifier getlabel( ) const;
         // The label of the formula mentioned above in case there is one.

         // There are of course many more attributes, but the others are
         // too much specialized to give them functions. 


   };


   // This error happens when we try try to abstract a formula into
   // a definition or a declaration: 

   struct cannotabstractform 
   {
      formula f; 
      property prop;

      cannotabstractform( formula f, const property& prop )
         : f(f), prop( prop ) 
      {
      }  

   };


   // And this one happens when you try to abstract a declaration that is 
   // not a 0-arity function declaration into another declaration. This 
   // cannot be done, because in first-order logic functions or predicates
   // cannot be parameters. 

   struct cannotabstractdecl
   {
      declaration d;
      property prop;

      cannotabstractdecl( declaration d, const property& prop )
         : d(d), prop( prop )
      {
     }

   };


   // Thrown by checkdeclarations( );


   struct undeclaredidentifier
   {
      declaration d;
      formula f;

      undeclaredidentifier( const declaration& d, formula f )
         : d(d), f(f)
      {
      }

   }; 
   
private: 
   std::vector< property > repr;


public: 
   void assumefunction( identifier f, unsigned int arity, bool permanent );
      // Assume a function. Redeclarations are allowed. 

   void definefunction( identifier f, unsigned int arity, bool permanent,
                        const functionvalue& def );
      // Define a function through def.

   void definefunction( identifier f, unsigned int arity, bool permanent,
                        const identifier& lab, const functionvalue& def );
      // Define a function through def, and call the defining function 
      // lab. 

   void definefunction( identifier f, unsigned int arity, 
                        const predicatevalue& val, 
                        const std::string& proofexistence,
                        const std::string& proofuniqueness );
      // Always permanent.

   void definefunction( identifier f, unsigned int arity,
                        const identifier& lab, const predicatevalue& val,
                        const std::string& proofexistence,
                        const std::string& proofuniqueness );
      // Always permanent. 

   void assumepredicate( identifier p, unsigned int arity, bool permanent );
      // Assume a predicate.

   void definepredicate( identifier p, unsigned int arity, bool permanent,
                         const predicatevalue& pred );
      // Assume a predicate through def.

   void definepredicate( identifier p, unsigned int arity, bool permanent,
                         const identifier& lab, const predicatevalue& pred ); 
      // Assume a predicate through def, and call the defining equivalence
      // lab.


   void assumeformula( formula f, bool permanent );
      // Assume a formula.

   void assumeformula( identifier lab, formula f, bool permanent );
      // Assume a formula and call it lab.
 
   void proveformula( formula f, bool permanent, const std::string& proof );
      // Add a proven formula.

   void proveformula( identifier lab, formula f, 
                      bool permanent, const std::string& proof );
      // Add a proven formula and give a label to it. 
 
    

   // All lookup functions work through iterators. 
 
   std::vector< property > :: const_iterator
       getfunctiondef( identifier id, unsigned int arity ) const;
   std::vector< property > :: const_iterator
       getpredicatedef( identifier id, unsigned int arity ) const;
          // We return the most recent declaration.
          // When no declaration exists, end( ) is returned.

   std::vector< property > :: const_iterator 
      getformula( identifier lab, int offset ) const;
   std::vector< property > :: const_iterator 
      getformula( int offset ) const;
         // Produce an iterator to a context::property s.t.,
         // getformula( ) will produce the formula.
         // If for some reason the formula does not exist, we return
         // end( ). 
 
   std::vector< property > :: const_iterator begin( ) const;
   std::vector< property > :: const_iterator end( ) const;



   std::vector< property > :: const_iterator  
      findcapture( const std::vector< property > :: const_iterator f ) const;
         // f should be a declaration that has a formula. We want to use
         // this formula for proving another formula which will be 
         // appended at the end of the context. Since we allow
         // redeclaration of functions/predicates, it is possible that
         // a function/predicate, that is free in f -> getformula( ),
         // is redeclared at a later point. In that case f -> getformula( )
         // cannot be used, because the free variable would be caught
         // by the redeclaration. 
         // 
         // findcapture( ) checks for the existence of capturing 
         // declarations, and returns such declaraton if it exits.   
         // Otherwise, end( ) is returned (which is what one normally wants)
         // 
         // As an example, suppose that we have L : p(a) somewhere at the
         // beginning of the context. If we later declare FUNCTION a:0, then
         // reusing L for proving p(a) would be incorrect, because we 
         // would be able derive [ FUNCTION a:0 ] p(a) after that. 
     

   unsigned int size( ) const;
   void backtrack( unsigned int k ) 
            throw( cannotabstractform, cannotabstractdecl ); 
      // One should backtrack only to values that were returned by size( )
      // before. Backtracking up to k will delete all non-permanent
      // objects at positions >= k. Permanent objects are kept, but possibly 
      // moved, so that all iterators become useless. 

 
   void changescope( unsigned int k, bool permanent) ;
     // Change scope of assumed functions, predicates and formulas.
     // New scope is defined by boolean variable permanent. 
     // Modification stops at depth k. 
     // This function is only used to simplify parser. 

   bool contains( std::vector< property > :: const_iterator p,
                  declaration decl ) const;
      // True if context [ p .. end( ) ] contains the declaration decl.

   void rename( std::vector< property > :: iterator p,
                declaration decl, identifier val );
      // Rename in context [ p .. end( ) ] decl into val. 

   void substitute( std::vector< property > :: iterator p,
                    const substitution& subst );
      // Apply substitution on context [ p .. end( ) ].
      // We could have made this a method of substitute, but
      // it does so much inspection of context, that we left it here. 
      // 
      // We use (in appendatend) the fact that, when substituting
      // [ x := F ], any declaration that declares a free variable of
      // F, is renamed. In particular, this is also done, when x does 
      // not occur free in the context. 
 
   void abstract( std::vector< property > :: iterator p,
                  formula f ) throw( cannotabstractform );
      // Replace every formula F that occurs in context [ p .. end( ) ] by
      // f -> f. If we encounter a definition, or a declaration, we cannot
      // abstract. When the exception is thrown, it is possible to parts of 
      // this context have been modified. 

   void abstract( std::vector< property > :: iterator p,
                  declaration decl ) throw( cannotabstractdecl );


   void checkdeclarations( formula f ) const throw( undeclaredidentifier );
   void checkdeclarations( const functionvalue& f ) const
            throw( undeclaredidentifier );
   void checkdeclarations( const predicatevalue& p ) const
            throw( undeclaredidentifier ); 
      // Everything that we want to say, is said by the exception.


   static formula make_existence( const predicatevalue& val );
   static formula make_uniqueness( const predicatevalue& val ); 
      // Make existence and uniqueness condition from predicate value.
      // arity must be at least one.

   friend std::ostream& operator << ( std::ostream& , const context& ); 

private:
   std::vector< property > :: const_iterator
      getformula( identifier lab ) const;

   std::vector< property > :: const_iterator
      findoffset( std::vector< property > :: const_iterator p,
                  int offset ) const; 

   static declaration freshdecl( declaration decl, formula f ); 
      // Produces a declaration that does not occur in f 
      // (free or not free), and which has the same arity as decl. 

   declaration freshdecl( declaration decl, 
                          const std::vector< property > :: const_iterator p,
                          const substitution& subst ) const;
      // Derive from decl a declaration that does occur in
      // context [ p .. end( ) ], and also not in subst. 

   void checkdeclarations( 
             formula f, 
             std::list< declaration > & localdeclarations ) const 
   throw( undeclaredidentifier ); 

   void checkdeclarations( 
             term t, formula f, 
             const std::list< declaration > & localdeclarations ) const
   throw( undeclaredidentifier ); 
      // The formula is only used for the error. 
};


std::ostream& operator << ( std::ostream& , const context::funcassumption& );
std::ostream& operator << ( std::ostream& , 
                            const context::funcpreddefinition& );
std::ostream& operator << ( std::ostream& , const context::predassumption& );
std::ostream& operator << ( std::ostream& , const context::formassumption& );
std::ostream& operator << ( std::ostream& , const context::property& ); 

std::ostream& operator << ( std::ostream& , 
                            const context::cannotabstractform& );
std::ostream& operator << ( std::ostream& , 
                            const context::cannotabstractdecl& );
std::ostream& operator << ( std::ostream& ,
                            const context::undeclaredidentifier& u );

std::ostream& operator << ( std::ostream& , const context& c );


#endif


