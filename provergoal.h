

// Code written by Hans de Nivelle and Piotr Witkowski, May 2008.


#ifndef PROVERGOAL_INCLUDED  
#define PROVERGOAL_INCLUDED   1


#include "formula.h"
#include "tptp_namemap.h"


#include <list>
#include <string>
#include <stdlib.h>


// Thrown when formula f contains a higher-order quantification
// decl. One could argue that being first-order is a fundamental property
// that should be checked elsewhere, but we view it as a peculiarity of 
// TPTP.


struct notfirstorder
{
   formula f;
   declaration decl;

   notfirstorder( formula f, const declaration& decl )
      :f(f), decl(decl)
   {
   }

};


struct provergoal
{

   std::list< formula > axioms;   
   std::list< formula > hypotheses;

   formula conjecture;


   provergoal( const std::list< formula > & axioms,
               const std::list< formula > & hypotheses,
               formula conjecture )
      : axioms( axioms ),
        hypotheses( hypotheses ),
        conjecture( conjecture )
   {
   } 

   provergoal( formula conjecture )
      : conjecture( conjecture )
   {
   }

   
   void print_tptp( std::ostream& stream ) const throw( notfirstorder ); 

   static void print_tptp( std::ostream& stream, formula f, tptp_namemap& map )
                                    throw( notfirstorder );
   static void print_tptp( std::ostream& stream, term t, tptp_namemap& map )
                                    throw( notfirstorder ); 

};


std::ostream& operator << ( std::ostream& stream, const notfirstorder& nfo );
std::ostream& operator << ( std::ostream& stream, const provergoal& );


#endif 


