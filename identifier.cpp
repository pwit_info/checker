

#include "identifier.h"
#include "assert.h"

#include "identifier.stat"


namespace
{


   // Returns something of form sXXXX, where XXXX represents i.

   std::string addindex( std::string s, unsigned int i )
   {
      unsigned int dig = 1;
      while( dig <= i )
         dig *= 10;

      dig = dig / 10;

      while( dig != 0 )
      {
         unsigned int d = i / dig;
         i = i % dig;

         s. push_back( '0' + d );
         dig = dig / 10;
      }

      return s;
   }


   // Remove numbers at the end of a string. The result may be
   // the empty string.


   std::string stripdigits( const std::string& s )
   {
      std::string res;

      unsigned int k = s. size( );

   restart:
      if( k == 0 )
         return res;

      unsigned k1 = k - 1;
      if( s[ k1 ] >= '0' && s[ k1 ] <= '9' )
      {
         k = k1;
         goto restart;
      }

      for( unsigned int i = 0; i < k; ++ i )
         res += s[i];
      return res;
   }

}




identifier::identifier( const std::string &stringrepr )
{
   std::map< std::string, unsigned int > :: iterator
         p = lookuptable. find( stringrepr );

   if( p == lookuptable. end( ))
   {
      unsigned int s = strings. size( );
      strings. push_back( stringrepr );
      lookuptable. insert( make_pair( stringrepr, s ));
      repr = s;
   }
   else
      repr = ( p -> second );
}


identifier::identifier( const std::string& base, unsigned int index )
{
   std::string s = stripdigits( base );
   if( s. size( ) == 0 )
      s = 'V';

   s = addindex( s, index );

   *this = identifier::identifier(s);
      // I find this ugly, but it seems to be legal. 
}


void identifier::printtable( std::ostream& stream )
{
   std::cout << "table of identifiers:\n";

   for( std::vector< std::string > :: const_iterator
           p = strings. begin( );
           p != strings. end( );
           ++ p )
   {
      std::cout << "   " << *p << "\n";
      ASSERT( lookuptable [*p ] == ( p - strings. begin( )));
   }
}



