
// This code was produced by Maphoon 2008.
// Code of struct token:


#include "token.h"


bool token::iswellformed( ) const
{
   switch( type )
   {
   case tkn__recover:
   case tkn__defaultred:
   case tkn_SCANERROR:
   case tkn_EOF:
   case tkn_LPAR:
   case tkn_RPAR:
   case tkn_LSQBR:
   case tkn_RSQBR:
   case tkn_LANGBR:
   case tkn_RANGBR:
   case tkn_COMMA:
   case tkn_COLON:
   case tkn_COLONEQ:
   case tkn_LAMBDA:
   case tkn_MP:
   case tkn_BY:
   case tkn_INDIRECTLY:
   case tkn_IN:
   case tkn_END:
   case tkn_AND:
   case tkn_OR:
   case tkn_IMPLIES:
   case tkn_EQUIV:
   case tkn_NOT:
   case tkn_TRUE:
   case tkn_FALSE:
   case tkn_MINUS:
   case tkn_PLUS:
   case tkn_EQ:
   case tkn_PROVE:
   case tkn_FROM:
   case tkn_ASSUME:
   case tkn_DEFINE:
   case tkn_EXISTENCE:
   case tkn_UNIQUENESS:
   case tkn_FUNCTION:
   case tkn_PREDICATE:
   case tkn_FORMULA:
   case tkn_Proof:
   case tkn_Prove:
   case tkn_Permanent_Definition:
   case tkn_Assumptions:
   case tkn_Permanent_Assumptions:
   case tkn_Subproof:
   case tkn_ASSUMERed:
   case tkn_Declarations:
   case tkn_Declaration:
   case tkn_Assume_Defined_Predicate:
   case tkn_Assume_Defined_Function:
   case tkn_Define_Predicate:
   case tkn_Define_Function_byPred:
   case tkn_Define_Function_byTerm:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_IDENTIFIER:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) < 1 ) return false;
      if( id. size( ) >= 2 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_UNSIGNED:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) < 1 ) return false;
      if( uint. size( ) >= 2 ) return false;
      return true;
   case tkn_Labeled_Formula:
   case tkn_Formula_Declaration:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) < 1 ) return false;
      if( f. size( ) >= 2 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 2 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_References:
      if( d. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Function_Declaration:
   case tkn_Predicate_Declaration:
      if( d. size( ) < 1 ) return false;
      if( d. size( ) >= 2 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Lambda_Bound_Vars:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Labeled_Term:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 2 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) < 1 ) return false;
      if( t. size( ) >= 2 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Reference:
   case tkn_Formula:
   case tkn_Atom:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) < 1 ) return false;
      if( f. size( ) >= 2 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Instantiations:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) < 1 ) return false;
      if( subst. size( ) >= 2 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Term:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) < 1 ) return false;
      if( t. size( ) >= 2 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Ident_list:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) < 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Bound_list:
   case tkn_QForall:
   case tkn_QExists:
      if( d. size( ) < 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) >= 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   case tkn_Term_list:
      if( d. size( ) >= 1 ) return false;
      if( f. size( ) >= 1 ) return false;
      if( id. size( ) >= 1 ) return false;
      if( idents. size( ) >= 1 ) return false;
      if( label. size( ) >= 1 ) return false;
      if( subst. size( ) >= 1 ) return false;
      if( t. size( ) < 1 ) return false;
      if( uint. size( ) >= 1 ) return false;
      return true;
   }
   return false; // Because of unknown type.
}


// If you see a very big error message originating from
// this point, then the most probably reason is that
// one of the attribute types has no print function defined.

std::ostream& operator << ( std::ostream& stream, const token& t )
{
   switch( t. type )
   {
   case tkn__recover:
      stream << "_recover( "; break;
   case tkn__defaultred:
      stream << "_defaultred( "; break;
   case tkn_SCANERROR:
      stream << "SCANERROR( "; break;
   case tkn_EOF:
      stream << "EOF( "; break;
   case tkn_IDENTIFIER:
      stream << "IDENTIFIER( "; break;
   case tkn_UNSIGNED:
      stream << "UNSIGNED( "; break;
   case tkn_LPAR:
      stream << "LPAR( "; break;
   case tkn_RPAR:
      stream << "RPAR( "; break;
   case tkn_LSQBR:
      stream << "LSQBR( "; break;
   case tkn_RSQBR:
      stream << "RSQBR( "; break;
   case tkn_LANGBR:
      stream << "LANGBR( "; break;
   case tkn_RANGBR:
      stream << "RANGBR( "; break;
   case tkn_COMMA:
      stream << "COMMA( "; break;
   case tkn_COLON:
      stream << "COLON( "; break;
   case tkn_COLONEQ:
      stream << "COLONEQ( "; break;
   case tkn_LAMBDA:
      stream << "LAMBDA( "; break;
   case tkn_MP:
      stream << "MP( "; break;
   case tkn_BY:
      stream << "BY( "; break;
   case tkn_INDIRECTLY:
      stream << "INDIRECTLY( "; break;
   case tkn_IN:
      stream << "IN( "; break;
   case tkn_END:
      stream << "END( "; break;
   case tkn_AND:
      stream << "AND( "; break;
   case tkn_OR:
      stream << "OR( "; break;
   case tkn_IMPLIES:
      stream << "IMPLIES( "; break;
   case tkn_EQUIV:
      stream << "EQUIV( "; break;
   case tkn_NOT:
      stream << "NOT( "; break;
   case tkn_TRUE:
      stream << "TRUE( "; break;
   case tkn_FALSE:
      stream << "FALSE( "; break;
   case tkn_MINUS:
      stream << "MINUS( "; break;
   case tkn_PLUS:
      stream << "PLUS( "; break;
   case tkn_EQ:
      stream << "EQ( "; break;
   case tkn_PROVE:
      stream << "PROVE( "; break;
   case tkn_FROM:
      stream << "FROM( "; break;
   case tkn_ASSUME:
      stream << "ASSUME( "; break;
   case tkn_DEFINE:
      stream << "DEFINE( "; break;
   case tkn_EXISTENCE:
      stream << "EXISTENCE( "; break;
   case tkn_UNIQUENESS:
      stream << "UNIQUENESS( "; break;
   case tkn_FUNCTION:
      stream << "FUNCTION( "; break;
   case tkn_PREDICATE:
      stream << "PREDICATE( "; break;
   case tkn_FORMULA:
      stream << "FORMULA( "; break;
   case tkn_Proof:
      stream << "Proof( "; break;
   case tkn_Prove:
      stream << "Prove( "; break;
   case tkn_Permanent_Definition:
      stream << "Permanent_Definition( "; break;
   case tkn_Assumptions:
      stream << "Assumptions( "; break;
   case tkn_Permanent_Assumptions:
      stream << "Permanent_Assumptions( "; break;
   case tkn_Subproof:
      stream << "Subproof( "; break;
   case tkn_Labeled_Formula:
      stream << "Labeled_Formula( "; break;
   case tkn_References:
      stream << "References( "; break;
   case tkn_ASSUMERed:
      stream << "ASSUMERed( "; break;
   case tkn_Declarations:
      stream << "Declarations( "; break;
   case tkn_Declaration:
      stream << "Declaration( "; break;
   case tkn_Function_Declaration:
      stream << "Function_Declaration( "; break;
   case tkn_Predicate_Declaration:
      stream << "Predicate_Declaration( "; break;
   case tkn_Formula_Declaration:
      stream << "Formula_Declaration( "; break;
   case tkn_Assume_Defined_Predicate:
      stream << "Assume_Defined_Predicate( "; break;
   case tkn_Assume_Defined_Function:
      stream << "Assume_Defined_Function( "; break;
   case tkn_Lambda_Bound_Vars:
      stream << "Lambda_Bound_Vars( "; break;
   case tkn_Labeled_Term:
      stream << "Labeled_Term( "; break;
   case tkn_Define_Predicate:
      stream << "Define_Predicate( "; break;
   case tkn_Define_Function_byPred:
      stream << "Define_Function_byPred( "; break;
   case tkn_Define_Function_byTerm:
      stream << "Define_Function_byTerm( "; break;
   case tkn_Reference:
      stream << "Reference( "; break;
   case tkn_Instantiations:
      stream << "Instantiations( "; break;
   case tkn_Term:
      stream << "Term( "; break;
   case tkn_Formula:
      stream << "Formula( "; break;
   case tkn_Ident_list:
      stream << "Ident_list( "; break;
   case tkn_Bound_list:
      stream << "Bound_list( "; break;
   case tkn_QForall:
      stream << "QForall( "; break;
   case tkn_QExists:
      stream << "QExists( "; break;
   case tkn_Atom:
      stream << "Atom( "; break;
   case tkn_Term_list:
      stream << "Term_list( "; break;
   default:
      stream << "UNKNOWNTOKEN( ";
   }

   unsigned int arg = 0;
   unsigned int arginlist;

   arginlist = 0;
   for( std::list< declaration > :: const_iterator
           p = t. d. begin( );
           p != t. d. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< formula > :: const_iterator
           p = t. f. begin( );
           p != t. f. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< identifier > :: const_iterator
           p = t. id. begin( );
           p != t. id. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< identifier > :: const_iterator
           p = t. idents. begin( );
           p != t. idents. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< identifier > :: const_iterator
           p = t. label. begin( );
           p != t. label. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< substitution > :: const_iterator
           p = t. subst. begin( );
           p != t. subst. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< term > :: const_iterator
           p = t. t. begin( );
           p != t. t. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   arginlist = 0;
   for( std::list< unsigned int > :: const_iterator
           p = t. uint. begin( );
           p != t. uint. end( );
           ++ p )
   {
      if( arg != 0 && arginlist == 0 ) stream << "; ";
      if( arg != 0 && arginlist != 0 ) stream << ", ";
      stream << *p;
      ++ arg;
      ++ arginlist;
   }

   stream << " )";
   return stream;
}


