

#include "prover.h"

#include <iostream>
#include <fstream> 
#include <deque> 


prover::prover( )
   : filecounter(0), 
     usefilecounter( true ),
     runprover( true ) 
{ } 


std::string prover::createcommand( const std::string& command,
                                   const std::string& inputfile,
                                   const std::string& outputfile,
                                   const std::string& timeout )
{
   std::string result;

   unsigned int position = 0;
   while( position + 1 < command. size( ))
   {
      if( command [ position ] == '%' )
      {
         switch( command [ position + 1 ] )
         {
         case 'i':
         case 'I':
            result += inputfile;
            position += 2;
            break;
         case 't':
         case 'T':
            result += timeout;
            position += 2;
            break;
         case 'o':
         case 'O':
            result += outputfile; 
            position += 2;
            break;
         case 'n':
         case 'N':
            result += '\n';
            position += 2; 
         default:
            result += command [ position ];
            ++ position;
         }
      }
      else
      {
         result. push_back( command [ position ] ); 
         ++ position;
      }
   }

   while( position < command. size( ))
   {
      result. push_back( command [ position ] );
      ++ position;
   }

   return result; 
}


namespace
{

   // Returns something of form sXXXXXX, where XXXXXX represents i.

   std::string addindex( std::string s, unsigned int i )
   {
      unsigned int dig = 1000000;
      while( dig <= i )
         dig *= 10;

      dig = dig / 10;

      while( dig != 0 )
      {
         unsigned int d = i / dig;
         i = i % dig;

         s. push_back( '0' + d );
         dig = dig / 10;
      }

      return s;
   }

}


bool prover::trytoprove( const provergoal& pg ) 
   throw( readerror, writeerror, notfirstorder ) 
{
   std::string inputfile;
   if( usefilecounter )
      inputfile = addindex( inputtemplate, filecounter );
   else
      inputfile = inputtemplate;

   // Inputfile is an input file for the theorem prover.
   // For us it's an outputfile.

   std::ofstream inp( inputfile. c_str( ));
   if( ! inp ) 
      throw writeerror::writeerror( inputfile ); 

   pg. print_tptp( inp );

   inp. close( );

   std::string outputfile;
   if( usefilecounter )
      outputfile = addindex( outputtemplate, filecounter ); 
   else
      outputfile = outputtemplate;

   std::string command = createcommand( this -> command, 
                                        inputfile, 
                                        outputfile, 
                                        this -> timeout );

   bool b = false; 

   if( runprover )
   {
      int returncode = system( command. c_str( )); 
         // We assume that command can contain '\n'-s.

      if( returncode != 0 )
         throw readerror::readerror( command );
            // This is abuse of readerror of course, but a wrong error 
            // is better than no error at all.

      b = containsacceptingstring( outputfile );
   }
   else
   {
      std::cerr << "warning: accepting goal without check\n";
      b = true;
   } 

   if( usefilecounter )
      ++ filecounter;

   return b; 
}


prover prover::readprover( const std::string& filename ) throw( readerror )
{
   std::ifstream inp( filename. c_str( ));
   if( !inp )
      throw readerror::readerror( filename );

   prover pr;

   getline( inp, pr. name );
   getline( inp, pr. inputtemplate ); 
   getline( inp, pr. outputtemplate ); 
   getline( inp, pr. command );
   getline( inp, pr. acceptingstring );
   inp >> pr. usefilecounter; 
   inp >> pr. runprover;
   getline( inp, pr. timeout );   // This is an ugly hack.
                                  // inp >> pr. runprover leaves 
                                  // the file reader after the integer 
                                  // at the position of the new line.
                                  // In order to get the timeout string,
                                  // one has to skip this newline first. 
   getline( inp, pr. timeout );
 
   if( !inp )
      throw readerror::readerror( filename );

   return pr; 
}
 
 
bool prover::containsacceptingstring( const std::string& filename ) const
throw( readerror ) 
{
   std::ifstream f( filename. c_str( ));
   if( !f )   
      throw readerror::readerror( filename );

   std::deque< char > q;

   char nextchar = f. get( );

   while( true ) 
   {
      if( q. size( ) >= acceptingstring. size( ))
      {
         std::deque< char > :: const_iterator p1 = q. begin( );  
         std::string::const_iterator p2 = acceptingstring. begin( );

         while( p2 != acceptingstring. end( ) && *p1 == *p2 )
         {
            ++ p1;
            ++ p2;
         }

         if( p2 == acceptingstring. end( ))
            return true; 

         q. pop_front( );  // q is guarenteed to be non-empty because
                           // empty acceptingstring while be accepted. 
      }
      else
      {
         if( nextchar == EOF )
            return false;

         q. push_back( nextchar ); 
         if(f)
            nextchar = f. get( );
         else
            throw readerror::readerror( filename ); 
      }
   }
}


std::ostream& operator << ( std::ostream& stream, const prover &pr )
{
   stream << "Prover Information:\n";
   stream << "------------------\n\n";

   stream << "name =              " << pr. name << "\n"; 
   stream << "input template =    " << pr. inputtemplate << "\n";
   stream << "output template =   " << pr. outputtemplate << "\n";
   stream << "command =           " << pr. command << "\n";
   stream << "accepting string =  " << pr. acceptingstring << "\n";
   stream << "file counter =      " << pr. filecounter << "\n";
   stream << "use file counter =  " << pr. usefilecounter << "\n";
   stream << "run prover =        " << pr. runprover << "\n";
   stream << "time out =          " << pr. timeout << "\n";
   stream << "\n\n";
   return stream;
}
 

std::ostream& operator << ( std::ostream& stream, 
                            const prover::readerror& err )
{
   stream << "a problem occurred while opening or reading from file ";
   stream << err. f << "\n";
   return stream;
}


std::ostream& operator << ( std::ostream& stream,
                            const prover::writeerror& err )
{
   stream << "a problem occurred while opening or writing into file ";
   stream << err. f << "\n";
   return stream;
}


