

#ifndef IDENTIFIER_INCLUDED
#define IDENTIFIER_INCLUDED   1


#include <string>
#include <map>
#include <vector> 


#include "assert.h"

#include <iostream>



class identifier
{


   static std::map< std::string, unsigned int > lookuptable;
      // assigns numbers to strings in order to ease comparisons,
      // and to make assignment more efficient.

   static std::vector< std::string > strings;


   unsigned int repr; 


   identifier( );
      // there is no default constructor
 
public: 
   identifier( const std::string& stringrepr );

   identifier( const std::string& base, unsigned int index );
      // Constructs a string which has approximately the form
      // base+index. In case base ends with digits, these
      // digits are removed. Therefore, it is somewhat unpredictable
      // which identifier will be actually constructed. But this
      // is not really important, as long as the new identifier is checked
      // for freshness. 
 
 
   identifier( const identifier &other ) 
      : repr( other. repr )
   {
   }


   const std::string& getstring( ) const
   {
      return strings [ repr ]; 
   }  


   inline void operator = (identifier other)
   {
     repr = other. repr ;
   }


   static void printtable( std::ostream& stream );


   friend bool operator == ( identifier f1, identifier f2 );
   friend bool operator != ( identifier f1, identifier f2 );
   friend bool operator < ( identifier f1, identifier f2 );
   friend bool operator > ( identifier f1, identifier f2 );
   friend bool operator <= ( identifier f1, identifier f2 );
   friend bool operator >= ( identifier f1, identifier f2 );

   friend std::ostream& operator << ( std::ostream& , const identifier& );
};


 
 

inline bool operator == ( identifier f1, identifier f2 )
{
   return f1. repr == f2.repr; 
}


inline bool operator != ( identifier f1, identifier f2 )
{
   return f1. repr != f2. repr;
}


inline bool operator < ( identifier f1, identifier f2 )
{
   return f1. repr < f2. repr ;
}


inline bool operator > ( identifier f1, identifier f2 )
{
   return f1. repr > f2. repr ;
}


inline bool operator <= ( identifier f1, identifier f2 )
{
   return f1. repr <= f2. repr;
}


inline bool operator >= ( identifier f1, identifier f2 )
{
   return f1. repr >= f2. repr;
}


inline std::ostream& operator << ( std::ostream& stream, const identifier& f )
{
   stream << f. getstring( ); 
   return stream;
}   


#endif
