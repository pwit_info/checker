

// Code written by Hans de Nivelle, May 2008.


#ifndef TPTP_MAP_INCLUDED  
#define TPTP_MAP_INCLUDED   1


#include "formula.h"


// A tptp_namemap maps our own identifiers into tptp_identifiers.
// This mapping is necessary because of two reasons:
// (1) We are much more liberal about identifiers than TPTP.
//     In tptp, identifiers must be alphanumeric only. Quantified
//     variables must start with a capital, free variables must
//     start with a non-capital. 
// (2) We allow the same identifiers to occur with different arities,
//     and to occur both as predicate and as function symbol.
//     This is not allowed in TPTP.


class tptp_namemap
{


   struct boundvar
   {
      declaration decl;
      std::string val;

      boundvar( const declaration& decl, const std::string& val )
         : decl( decl ), val( val )
      {
      }

   };


   // This class defines an order on declarations. This is required
   // in order to be able to build a map of declarations.

   struct declaration_comparator 
   {
      bool operator ( ) ( const declaration& decl1, const declaration& decl2 )
      {
         if( decl1. var < decl2. var )
            return true;

         if( decl1. var == decl2. var && decl1. arity < decl2. arity )
            return true;

         if( decl1. var == decl2. var && decl1. arity == decl2. arity &&
             decl1. isfunction && ! decl2. isfunction )  
            return true;
         
         return false;
      }

   };


   std::map< declaration, std::string, declaration_comparator > global;
   std::map< std::string, bool > usedstrings;

   std::list< boundvar > local;
      // Most recent declaration is in front. 

public:  

   const std::string& lookup( const declaration& decl );
      // Map the declaration into a unique string. 
      // First, we check if decl occurs in local, simply by walking
      // through it. If it does not, we check in global.
      // If it does not occur in global, we create an entry, in global,
      // starting with non-capital.

   const std::string& declarelocal( const declaration& decl );
      // Declare a local variable. We map it into a unique string
      // starting with a capital, and put it in local. 

   unsigned int nrlocaldeclarations( ) const;
      // Number of local declarations.

   void backtrack( unsigned int k );
      // Backtracks local names. In the code, there are two options:
      // Either remove backtracked names from usedstrings, or leave them.
      // In the former case, local names can be reused. 


   static std::string legalize( const std::string& s, bool uppercase );
      // Legalize s into a string that is allowed by TPTP.
      // If uppercase == true, the resulting string will start with an 
      // uppercase letter. 
 
   const std::string& makefreshstring( const std::string& s, bool uppercase );
      // First legalize s, after add (if required) an index to the result, so  
      // that the result will be a fresh string. The result is inserted
      // into usedstrings, and a reference to this inserted string is 
      // returned. 


   friend std::ostream& operator << ( std::ostream& stream,
                                      const tptp_namemap& map ); 

};


std::ostream& operator << ( std::ostream& stream, const tptp_namemap& map ); 


#endif

