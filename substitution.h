

// Written by Hans de Nivelle & Piotr Witkowski, May 2008.


#ifndef SUBSTITUTION_INCLUDED
#define SUBSTITUTION_INCLUDED   1


#include "formula.h"
#include "values.h"


struct substitution
{

   // Assignment of form f -> lambda x1,..,xk. t:

   struct funcassignment
   {
      declaration var;
      functionvalue val;

      funcassignment( declaration var, functionvalue val )
         : var( var ), val( val )
       {  } 
   };

   
   // Assignment of form p -> lambda x1, ..., xk. p: 

   struct predassignment
   {
      declaration var;
      predicatevalue val;  

      predassignment( declaration var, predicatevalue val )
         : var( var ), val( val )
      {  }

   }; 


   // Substitutions are always simultaneous:


private: 
   std::list< funcassignment > functions;
   std::list< predassignment > predicates;

public: 
   void assign_function( identifier id, unsigned int arity, 
                         functionvalue val );
   void assign_predicate( identifier id, unsigned int arity,
                          predicatevalue val );
//   void assign_function( std::string id, unsigned int arity, 
//                         functionvalue val );
//   void assign_predicate( std::string id, unsigned int arity,
//                          predicatevalue val );

   const functionvalue* lookup_function( identifier id, 
                                         unsigned int arity ) const;
   const predicatevalue* lookup_predicate( identifier id,
                                           unsigned int arity ) const;
      // 0-pointer in case no value exists. 


   static bool isfree( declaration decl, term t );
   static bool isfree( declaration decl, formula f );
   static bool isfree( declaration decl, const functionvalue& f );
   static bool isfree( declaration decl, const predicatevalue& p );

   bool indomain( declaration decl ) const;
   bool free_in_range( declaration decl ) const;
   
   bool isempty( ) const;

   static bool contains( declaration decl, term t );
   static bool contains( declaration decl, formula f );
      // The difference between isfree and contains is that in 
      // contains also bound occurrences are taken into account. 
      // So x is contained in [x] p(x), in [x] p(y), in
      // [y] p(x).
      // x is only free in the last formula.
 
   static term rename( declaration decl, term t, identifier val );
   static formula rename( declaration decl, formula f, identifier val );  
      // The difference between rename and substitute, is that rename
      // also replaces bound occurrences. 

      // Checks if *this and s have distinct domains
   bool unionable_with( const substitution &s);
 
     // Create union of two distinct substitutions.
   substitution union_with( const substitution &s);


   term apply_on( term t ) const;  
   formula apply_on( formula f ) const;
   functionvalue apply_on( functionvalue f ) const;
   predicatevalue apply_on( predicatevalue p ) const;


// Let val = lambda x1,..,xk t'. Let t = t' [ t1,..,tk / x1,...,xk]
// Return substitution [ t1,..,tk / x1,...,xk]
   static substitution expansion( functionvalue val, term t );
   static substitution expansion( predicatevalue val, formula f );
   static bool alphaequal( const formula &f1, const formula &f2 );

private:

   static bool occurs( declaration decl, formula f, bool checkingfree );
      // occurs is the common implementation of isfree( ) and contains( ). 
      // If checkingfree, we are doing isfree( ), otherwise contains( ).

   friend std::ostream& operator << ( std::ostream& , const substitution& );
};


std::ostream& operator << ( std::ostream& , const substitution& );


inline bool substitution::isempty( ) const 
{
   return functions. empty( ) && predicates. empty( );
}


#endif 


