

// Hans de Nivelle & Piotr Witkowski, May 2008. 

#include <stdlib.h>

#include "substitution.h"



using namespace std;


void substitution::assign_function( identifier id, unsigned int arity,
                                    functionvalue val )
{
   ASSERT( arity == val. getarity( ));
   ASSERT( val. iswellformed( ));

   funcassignment fncassgn = funcassignment::funcassignment( 
                        declaration::make_funcdecl( id, arity ), val );
   functions. push_back( fncassgn );
}


void substitution::assign_predicate( identifier id, unsigned int arity,
                                     predicatevalue val )
{
   ASSERT( arity == val. getarity( ));
   ASSERT( val. iswellformed( ));

   predassignment predassgn = predassignment::predassignment(
                        declaration::make_preddecl( id, arity ), val );
   predicates. push_back( predassgn );
}

//void substitution::assign_function( std::string id, unsigned int arity, 
//                         functionvalue val )
//{
//   assign_function( identifier( id ), arity, val );
//}
	
//void substitution::assign_predicate( std::string id, unsigned int arity,
//                                     predicatevalue val )
//{
//   assign_predicate( identifier( id ), arity, val );
//}
 
const functionvalue* substitution::lookup_function ( identifier id, 
                                                    unsigned int arity ) const
{
   declaration tobefound = declaration::make_funcdecl( id, arity);

   for( list< funcassignment>:: const_iterator it = functions. begin( ); 
        it != functions. end( );
	it ++ )
   {
      if ( it -> var == tobefound )  
         return &(it -> val);
   }

   return NULL;
}


const predicatevalue* substitution::lookup_predicate ( identifier id,
                                                   unsigned int arity ) const
{
   declaration tobefound = declaration::make_preddecl( id, arity );

   for( list< predassignment > :: const_iterator it = predicates. begin( ); 
        it != predicates. end( );
	it ++ )
   {
      if ( it -> var == tobefound )  
         return &(it -> val);
   }

   return NULL;
}


bool substitution::isfree( declaration decl, term t )
{
   if( ! decl. isfunction )
      return false;
 
   if( t. getfunction( ) == decl. var &&
       t. getnrsubterms( ) == decl. arity )
   {
      return true;
   }
 
   for( list< term > :: const_iterator 
           it = t. subterms_begin( );
           it != t. subterms_end( );
           ++ it )
   {
      if( isfree( decl, *it )) 
         return true;
   }

   return false;
}


bool substitution::isfree( declaration decl, formula f )
{
   return occurs( decl, f, true );
}


bool substitution::isfree( declaration decl, const functionvalue& f )
{
   if( decl. isfunction && decl. arity == 0 ) 
   {
      for( std::list< identifier > :: const_iterator 
              v = f. boundvars. begin( );
              v != f. boundvars. end( );
              ++ v )
      {
         if( *v == decl. var )
            return false;
      }
   }

   return isfree( decl, f. t );
}


bool substitution::isfree( declaration decl, const predicatevalue& p )
{
   if( decl. isfunction && decl. arity == 0 )
   {
      for( std::list< identifier > :: const_iterator
              v = p. boundvars. begin( );
              v != p. boundvars. end( );
              ++ v )
      {
         if( *v == decl. var )
            return false;
      }
   }

   return isfree( decl, p. f );
}


bool substitution::indomain( declaration decl ) const
{
   if( decl. isfunction )
      return lookup_function( decl. var, decl. arity );
   else
      return lookup_predicate( decl. var, decl. arity );
}


bool substitution::free_in_range( declaration decl ) const
{
   for( std::list< funcassignment > :: const_iterator
           p = functions. begin( );
           p != functions. end( );
           ++ p )
   {
      if( isfree( decl, p -> val ))
         return true;
   }

   for( std::list< predassignment > :: const_iterator
           p = predicates. begin( );
           p != predicates. end( );
           ++ p )
   {
      if( isfree( decl, p -> val ))
         return true;
   }

   return false;
}


bool substitution::contains( declaration decl, term t )
{
   return isfree( decl, t );  // Without binders, it's the same. 
}


bool substitution::contains( declaration decl, formula f )
{
   return occurs( decl, f, false );
}


term substitution::rename( declaration decl, term t, identifier val )
{
   if( ! decl. isfunction )
      return t;
   else
   {
      identifier head = t. getfunction( );

      if( decl. var == head && decl. arity == t. getnrsubterms( ))
      {
         head = val;
      }
   
      list< term > subterms;

      for( list< term> :: const_iterator it = t. subterms_begin( );
           it != t. subterms_end( );
	   it ++ )
      {
         subterms. push_back( rename( decl, *it, val ));
      }

      return term::term( head, subterms );
   }
}


formula substitution::rename( declaration decl, formula f, identifier val )
{
   switch( f. getoperator( ))
   {
   case fol_false:
   case fol_true:
      return f;

   case fol_not:
   case fol_and:
   case fol_or:
   case fol_implies:
   case fol_equiv:
      {
         list< formula> subformulas = list< formula > ::list( );
         for( list< formula> :: const_iterator 
                 it = f. subformulas_begin( ); 
                 it != f. subformulas_end( ); 
	         it ++ )
         {
            subformulas. push_back( rename( decl, *it, val ));
         }
       
         switch( f. getoperator( ))
         { 
         case fol_not:
            return formula::make_not( subformulas. front( ));
         case fol_implies:
            return formula::make_implies( subformulas. front( ), 
                                          subformulas. back( ));
         case fol_equiv:
            return formula::make_equiv( subformulas. front( ), 
                                        subformulas. back( ));
         case fol_and:
            return formula::make_and( subformulas );
         case fol_or:
            return formula::make_or( subformulas );
         }
         ASSERT( false ); exit(0);
      }

   case fol_forall:
   case fol_exists:
      {
         list< declaration > variables = list< declaration >::list ( );

         for( list< declaration > :: const_iterator 
                 it = f. variables_begin( );  
	         it != f. variables_end( ); 
	         it ++ )
	 {
	    if ( *it == decl )
	    {
	       if ( decl. isfunction )
                  variables. push_back( declaration :: 
		                         make_funcdecl( val, decl.arity ));
	       else
	          variables. push_back( declaration :: 
		                          make_preddecl( val, decl. arity ));
	    }
	    else
	       variables. push_back( *it );
         }

         formula sub = rename( decl, *( f. subformulas_begin( )), val ); 
        
         if( f. getoperator( ) == fol_forall )
            return formula::make_schematicforall( variables, sub );
         else
            return formula::make_schematicexists( variables, sub ); 
      }

   case fol_equals:
      if( decl. isfunction ) 
      {
         std::list< term > :: const_iterator p = f. subterms_begin( );

         term t1 = rename( decl, *( p ++ ), val ); 
	 term t2 = rename( decl, *( p ++ ), val ); 
         return formula::make_equals( t1, t2 );
      } 
      else
        return f;

   case fol_atom:
      if ( decl. isfunction )
      {
         list< term> subterms;
  
         for ( list< term > :: const_iterator it = f. subterms_begin( );
	          it != f. subterms_end( );
		  it ++ )
	 {
            subterms. push_back( rename( decl, *it, val));
	 }

         return formula::make_atom( f. getpredicate( ), subterms);
      }
      else
      {
         unsigned int arity = 0;
         std::list< term > subterms;
 
         for ( list< term > :: const_iterator it = f. subterms_begin( );
	          it != f. subterms_end( );
	          it ++ )
	 {
	    arity ++;
            subterms. push_back( *it ); 
         }

	 if( f. getpredicate( ) == decl. var && arity == decl. arity )
         {
            return formula::make_atom( val, subterms );   
	 }
	 else
	    return f;
      } 
   }

   ASSERT( false ); exit(0); 
}


term substitution::apply_on( term t ) const
{
#if 0
   std::cout << "apply on " << t << "\n";
#endif

   if( functions. size( ))
   {
      const functionvalue* val = lookup_function( t. getfunction( ),
                                                  t. getnrsubterms( ));

      if( t. getnrsubterms( ) != 0 )  
      {
         std::list< term > subterms;

         for( std::list< term > :: const_iterator 
                 it = t. subterms_begin( );
                 it != t. subterms_end( );
	         it ++ )
         {
            subterms. push_back( apply_on( *it ));
         }

         term res = term::term( t. getfunction( ), subterms );

         if( val )
         {
            substitution m = expansion( *val, res );
            res = m. apply_on( val -> t );
         }
         return res; 
      }
      else
      {
         if( val )
            return val -> t;
         else
            return t;
      } 
   }
   else
      return t;
}   


namespace
{
   bool occursinlist( declaration decl, const std::list< declaration > & list )
   {
      for( std::list< declaration > :: const_iterator
              p = list. begin( );
              p != list. end( );
              ++ p )
      {
         if( decl == *p )
            return true;
      }
      return false;
   }

}



formula substitution::apply_on( formula f ) const 
{
#if 0
   std::cout << "apply on " << f << "\n";
#endif

   switch( f. getoperator( ))
   {
   case fol_false:
   case fol_true:
      return f;

   case fol_not:
   case fol_and:
   case fol_or:
   case fol_implies:
   case fol_equiv:
      {
         std::list< formula > sub;
         for( list< formula > :: const_iterator 
                 it = f. subformulas_begin( ); 
                 it!= f. subformulas_end( ); 
	         it ++ )
         {
            sub. push_back( apply_on( *it )); 
         }

         switch( f. getoperator( ))
         {
         case fol_not:
            return formula::make_not( sub. front( ));
         case fol_implies:
            return formula::make_implies( sub. front( ), sub. back( ));
         case fol_equiv:
            return formula::make_equiv( sub. front( ), sub. back( ));
         case fol_and:
            return formula::make_and( sub );
         case fol_or:
            return formula::make_or( sub );
         }
         ASSERT( false ); exit(0);
      }
 
   case fol_forall:
   case fol_exists:
      {
         // We first read out the declarations:
 
         std::list< declaration > vars;  
	 for ( std::list< declaration > :: const_iterator 
                  v = f. variables_begin( );
	          v != f. variables_end( );
	          ++ v )
         {
            vars. push_back( *v ); 
	 }

         formula body = *( f. subformulas_begin( ));

         for( std::list< declaration > :: iterator
                 v = vars. begin( );
                 v != vars. end( );
                 ++ v )
         {
            // If this variable is free in the domain or the 
            // range of the substitution, we will replace it.

            if( indomain( *v ) || free_in_range( *v ))
            {
               // Possible capture, we are going to look for a new
               // variable.

               unsigned int index = 0;
               std::string base = v -> var. getstring( );

               declaration freshdecl = *v;
               freshdecl. var = identifier::identifier( base, index ); 

               while( contains( freshdecl, f ) || 
                      indomain( freshdecl ) || 
                      free_in_range( freshdecl ) ||
                      occursinlist( freshdecl, vars ))
               { 
                  ++ index; 
                  freshdecl. var = identifier::identifier( base, index ); 
               }
              
               declaration olddecl = *v;

               body = rename( olddecl, body, freshdecl. var );  
                  // Rename in body.

               // And rename in the rest of the present quantification,
               // including the point at which we discovered the conflict. 

               for( std::list< declaration > :: iterator 
                       w = v; 
                       w != vars. end( ); 
                       ++ w )
               {
                  if( *w == olddecl ) 
                     *w = freshdecl;
               }
            }
         }

         body = apply_on( body );

         if( f. getoperator( ) == fol_forall )
            return formula::make_schematicforall( vars, body ); 
         else
            return formula::make_schematicexists( vars, body );  
      }

   case fol_equals:
      {
         std::list< term > :: const_iterator p = f. subterms_begin( );
         term t1 = apply_on( *p++ );
         term t2 = apply_on( *p++ ); 
 
         return formula::make_equals( t1, t2 );  
      }
   case fol_atom:
      {
         list< term > subterms;
	 unsigned int arity = 0;
  
         for ( std::list< term > :: const_iterator 
                  it = f. subterms_begin( );
	          it != f. subterms_end( );
	          it ++ )
	 {
            subterms. push_back( apply_on( *it ));
	    ++ arity; 
	 }

         formula res = formula::make_atom( f. getpredicate( ), subterms );

         const predicatevalue* val = lookup_predicate( f. getpredicate( ),
                                                       arity );  
         if( val )
         {
            substitution m = expansion( *val, res );
            res = m. apply_on( val -> f );
         }
         return res;
      }
   }

   ASSERT( false ); exit(0); 
}


functionvalue substitution::apply_on( functionvalue f ) const
{
   // Our strategy is the following. We first build the formula
   // all vars. f = xxxx, then we call apply_on on this formula. 

   term t = term::term( identifier::identifier( "xxxx" ), 
                        std::list< term > :: list( ));

   formula form = formula::make_equals( f. t, t ); 
   form = formula::make_forall( f. boundvars, form );

   form = apply_on( form );

   std::list< identifier > boundvars;
   for( std::list< declaration > :: const_iterator
           v = form. variables_begin( );
           v != form. variables_end( );
           ++ v )
   {
      boundvars. push_back( v -> var );
   }

   form = * ( form. subformulas_begin( ) );  
   t = * ( form. subterms_begin( ));

   return functionvalue::functionvalue( boundvars, t );
}

                            
predicatevalue substitution::apply_on( predicatevalue p ) const
{
   // Our strategy is the following. We first build the formula
   // all vars. p. After that we call apply_on( ) on this formula.

   formula form = formula::make_forall( p. boundvars, p. f );

   form = apply_on( form );

   std::list< identifier > boundvars;
   for( std::list< declaration > :: const_iterator
           v = form. variables_begin( );
           v != form. variables_end( );
           ++ v )
   {
      boundvars. push_back( v -> var );
   }

   form = * ( form. subformulas_begin( ) );

   return predicatevalue::predicatevalue( boundvars, form );
}

            
substitution substitution::expansion( functionvalue val, term t )
{
   substitution result;
 
   std::list< identifier > :: const_iterator
           v = val. boundvars. begin( );
   std::list< term > :: const_iterator 
           a = t. subterms_begin( );

   while( v != val. boundvars. end( ))
   {
      ASSERT( a != t. subterms_end( ));
    
      result. assign_function( *v, 0, functionvalue::functionvalue( *a ));
      ++ a;
      ++ v;
   }
   ASSERT( a == t. subterms_end( ));
 
   return result;
}


substitution substitution::expansion( predicatevalue val, formula f )
{
   substitution result;
   
   ASSERT( f. getoperator( ) == fol_atom );

   std::list< identifier > :: const_iterator v = val. boundvars. begin( );
   std::list< term > :: const_iterator a = f. subterms_begin( ); 

   while( v != val. boundvars. end( ))
   {
      ASSERT( a != f. subterms_end( ));

      result. assign_function( *v, 0, functionvalue( *a )); 
      ++ a; 
      ++ v; 
   }

   ASSERT ( a == f. subterms_end( ));
  
   return result;  
}


std::ostream& operator << ( std::ostream &stream , const substitution &s  )
{
   stream << "Substitution[ ";
  
   for( list< substitution :: funcassignment > :: const_iterator it = 
        s. functions. begin( );
        it != s. functions. end( );
	it ++ )
   {
      if ( it != s. functions. begin( ))
         stream << ", ";
      stream << it -> var. var << " := " ;
     
      stream << ( it -> val );  
   }
   
   stream << "; ";

   for( list< substitution :: predassignment> :: const_iterator it = 
        s. predicates. begin( );
        it != s. predicates. end( );
	it ++ )
   {
      if ( it != s. predicates. begin( ))
         stream << ", ";
      stream << it -> var. var << " := " ;
      stream << ( it -> val );
   }

   stream << " ] ";
   return stream;
}


bool substitution::occurs( declaration decl, formula f, bool checkingfree )
{

#if 0
   std::cout << "checking occurs " << decl << " in " << f;
   std::cout << " with " << checkingfree << "\n";
#endif

   switch ( f. getoperator( ))
   {
   case fol_false:
   case fol_true:
      return false;

   case fol_not:
   case fol_and:
   case fol_or:
   case fol_implies:
   case fol_equiv:
      for( list< formula > :: const_iterator it = 
	       f. subformulas_begin( ); 
               it != f. subformulas_end( ); 
	       ++ it )
         {
            if( occurs( decl, *it, checkingfree ))
	       return true;
         }
         return false;

   case fol_forall:
   case fol_exists:
  
      // If the declaration decl occurs among the declarations of the
      // quantifier, and checkingfree==true, we return false, because
      // the occurrence is bound.
      // If checkingfree==false, we return true, because it is an 
      // an occurrence.

      for( list< declaration > :: const_iterator 
              it = f. variables_begin( );
	           it != f. variables_end( );
	           it ++ )
      {     
         if( *it == decl ) 
         { 
            if( checkingfree ) 
	       return false;
            else
               return true; 
	 }
      }

      return occurs( decl, *( f. subformulas_begin( )), checkingfree );

   case fol_equals:
   case fol_atom:
 
      // If decl is a predicate declaration, then the only chance
      // of finding it, is when f has form p(t1,...,tn), and p/n is
      // decl.

      if( ! decl. isfunction )
      {
         if( f. getoperator( ) == fol_atom )
         {
            unsigned int arity = 0;
            for( std::list< term > :: const_iterator
                    it = f. subterms_begin( );
                    it != f. subterms_end( );
                    ++ it )
            {
               ++ arity;
            }

            return 
               f. getpredicate( ) == decl. var && arity == decl. arity;
         }
         else
            return false;
      }
      else
      {
         // The declaration is a functional declaration, we look for it in
         // the subterms.   

         for( list< term > :: const_iterator it = f. subterms_begin( );
	      it != f. subterms_end( );
	      it ++ )
	 {
            if( isfree( decl, *it ))
	       return true;
	 }
	
	 return false;
      }
   }
   ASSERT( false );

}

bool substitution::unionable_with( const substitution &s)
{

 for( std::list< funcassignment> :: iterator it = functions. begin( );
        it != functions. end( );
	it ++ )
   {
     if ( s. indomain( it -> var))
        return false; 
   }

   for( std::list< predassignment> :: iterator it = predicates. begin( );
        it != predicates. end( );
	it ++ )
   {
     if ( s. indomain( it -> var))
        return false;
      
   }

   return true;
}


substitution substitution::union_with( const substitution &s)
{
   substitution result;

   result = s;

   for( std::list< funcassignment> :: iterator it = functions. begin( );
        it != functions. end( );
	it ++ )
   {
      ASSERT( ! s. indomain( it -> var));
      
      result. assign_function( (it -> var). var, (it -> var). arity, it -> val);
   }

   for( std::list< predassignment> :: iterator it = predicates. begin( );
        it != predicates. end( );
	it ++ )
   {
      ASSERT( ! s. indomain( it -> var));
      
      result. assign_predicate( it -> var. var, it -> var. arity, it -> val);
   }

   return result;
}





template < typename T>
bool samelength( typename std::list< T> ::const_iterator  a_begin,
                 typename std::list< T> :: const_iterator a_end,
                 typename std::list< T> :: const_iterator b_begin, 
	         typename std::list< T> :: const_iterator b_end)
{

unsigned int l1,l2;

l1 = l2 = 0;

while ( a_begin != a_end )
{
   l1++;
   a_begin++;
}

while ( b_begin != b_end )
{
   l2++;
   b_begin++;
}


return l1 == l2;
}

bool canberenamed( const formula &f1, const formula &f2 )
{
   if ( f1. getoperator( ) != f2. getoperator( ) )
      return false;

 //  bool b = 
//   samelength<declaration>( f1. variables_begin( ), f1. variables_end( ),
//                            f2. variables_begin( ), f2. variables_end( ))
//
//   &&
//   samelength<term>( f1. subterms_begin( ), f1. subterms_end( ),
//                            f2. subterms_begin( ), f2. subterms_end( ))
//   &&
//   samelength<formula>( f1. subformulas_begin( ), f1. subformulas_end( ),
//                            f2. subformulas_begin( ), f2. subformulas_end( ));

 
//   if ( !b ) return false;

   switch( f1. getoperator( ) )
   {
      case fol_false:
      case fol_true:
         return true;
      case fol_not:
         return canberenamed( *(f1. subformulas_begin( )), *(f2. subformulas_begin( )));
      case fol_and:
      case fol_or:     
      // Althrough class formula allows multiary 'and' and 'or' formulas, we only use binary ones
      case fol_implies:
      case fol_equiv:
      {
         bool b1 = canberenamed( *(f1. subformulas_begin( )), *(f2. subformulas_begin( )));
	 bool b2 = canberenamed( *(++ f1.subformulas_begin( )), *( ++ f2. subformulas_begin( )));
         return b1 && b2;
      }   
      case fol_forall:
      case fol_exists:
      {

        bool b = 
        samelength<declaration>( f1. variables_begin( ), f1. variables_end( ),
                                 f2. variables_begin( ), f2. variables_end( ));
       
        if  ( !b ) return false;

        std::list< declaration >:: const_iterator it1, it2;
        substitution s;

	it1 = f1. variables_begin( );
	it2 = f2. variables_begin( );

   
        while ( it1 != f1. variables_end( ))
	{
           if ( ( it1 -> isfunction != it2  -> isfunction ) ||
 	        ( it1 -> arity != it2 -> arity ) )
	      return false;

          
	   if ( it1 -> isfunction )
	   {
             s. assign_function(  it1 -> var, it1 -> arity, functionvalue::fromdecl( *it2 ));
	   }
	   else
	   {
             s. assign_predicate(  it1 -> var, it1 -> arity, predicatevalue::fromdecl( *it2 ));
	   }

           it1++;
	   it2++;
	}

        formula sf1 = * ( f1. subformulas_begin( ));
        formula sf2 = * ( f2. subformulas_begin( )); 
 
        sf1 = s. apply_on( sf1 );

        return canberenamed( sf1, sf2);
      }

      case fol_equals:
      
         return ( *(f1. subterms_begin( )) == *( f2. subterms_begin( )) &&
	        ( *( ++ f1. subterms_begin( )) == *( ++ f2. subterms_begin( ))));
   

      case fol_atom:
          return f1 == f2;


      default:
         ASSERT( false );
   }

}


bool substitution::alphaequal ( const formula &f1, const formula &f2 )
{
   return canberenamed( f1, f2 ) && canberenamed( f2, f1);
}












