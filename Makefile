Flags = -Wreturn-type -pedantic -pedantic-errors -Wundef
CPP = g++

checker: Makefile parser.o token.o reader.o substitution.o prover.o context.o values.o formula.o term.o identifier.o provergoal.o tptp_namemap.o
	$(CPP) $(Flags) -o checker token.o reader.o parser.o identifier.o term.o formula.o values.o substitution.o prover.o context.o provergoal.o tptp_namemap.o

parser.o: Makefile token.h reader.h parser.h parser.cpp
	$(CPP) -c $(Flags) parser.cpp -o parser.o

token.o: Makefile token.h token.cpp
	$(CPP) -c $(Flags) token.cpp -o token.o

reader.o: Makefile token.h reader.h reader.cpp
	$(CPP) -c $(Flags) reader.cpp -o reader.o

 
clean:
	rm checker parser.o reader.o token.o identifier.o term.o formula.o values.o substitution.o context.o prover.o provergoal.o tptp_namemap.o 


context.o: Makefile context.h formula.h substitution.h context.cpp
	$(CPP) -c $(Flags) context.cpp -o context.o

prover.o: Makefile prover.h provergoal.h prover.cpp
	$(CPP) -c $(Flags) prover.cpp -o prover.o

provergoal.o: Makefile provergoal.h formula.h tptp_namemap.h provergoal.cpp
	$(CPP) -c $(Flags) provergoal.cpp -o provergoal.o

tptp_namemap.o: Makefile tptp_namemap.h formula.h tptp_namemap.cpp
	$(CPP) -c $(FlagS) tptp_namemap.cpp -o tptp_namemap.o

substitution.o: Makefile formula.h values.h substitution.h substitution.cpp
	$(CPP) -c $(Flags) substitution.cpp -o substitution.o

values.o: Makefile identifier.h term.h formula.h values.h values.cpp
	$(CPP) -c $(Flags) values.cpp -o values.o

formula.o: Makefile formula.h term.h formula.cpp 
	$(CPP) -c $(Flags) formula.cpp -o formula.o

term.o:  term.h identifier.h term.cpp 
	$(CPP) -c $(Flags) term.cpp -o term.o

identifier.o: identifier.stat identifier.h assert.h identifier.cpp 
	$(CPP) -c $(Flags) identifier.cpp -o identifier.o


