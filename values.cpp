

#include "values.h"


unsigned int functionvalue::getarity( ) const
{
   return boundvars. size( );
}

 
bool functionvalue::iswellformed( ) const
{
   for( std::list< identifier > :: const_iterator
           p1 = boundvars. begin( );
           p1 != boundvars. end( );
           ++ p1 )
   {
      for( std::list< identifier > :: const_iterator
              p2 = boundvars. begin( );
              p2 != p1;
              ++ p2 )
      {
         if( *p1 == *p2 )
            return false;
      }
   }
   return true;
}


functionvalue functionvalue::fromdecl( declaration d)
{
   std::list< identifier > boundvars;

 for ( int i = 1; i <= d. arity; i++ )
 {
    boundvars. push_back( identifier( std::string("x"), i));
 }

 return functionvalue( boundvars, term::term( d. var,
                                              std::list< term > ::list( )));

}



std::ostream& operator << ( std::ostream& stream,
                            const functionvalue& val )
{
   if( val. boundvars. size( ))
   {
      stream << "lambda{ ";
      for( std::list< identifier > :: const_iterator
              p = val. boundvars. begin( );
              p != val. boundvars. end( );
              ++ p )
      {
         if( p != val. boundvars. begin( ))
            stream << ",";
         stream << *p;
      }

      stream << " }. ";
   }
   stream << val. t;
   return stream;
}


unsigned int predicatevalue::getarity( ) const
{
   return boundvars. size( );
}


bool predicatevalue::iswellformed( ) const
{
   for( std::list< identifier > :: const_iterator
           p1 = boundvars. begin( );
           p1 != boundvars. end( );
           ++ p1 )
   {
      for( std::list< identifier > :: const_iterator
              p2 = boundvars. begin( );
              p2 != p1;
              ++ p2 )
      {
         if( *p1 == *p2 )
            return false;
      }
   }
   return true;
}

predicatevalue predicatevalue::fromdecl( declaration d)
{
   std::list< identifier > boundvars;

 for ( int i = 1; i <= d. arity; i++ )
 {
    boundvars. push_back( identifier( std::string("x"), i));
 }

 return predicatevalue( boundvars, formula::make_atom( d. var,
                                            std::list< term > ::list( )));
}

std::ostream& operator << ( std::ostream& stream,
                            const predicatevalue& val )
{
   if( val. boundvars. size( ))
   {
      stream << "lambda{ ";
      for( std::list< identifier > :: const_iterator
              p = val. boundvars. begin( );
              p != val. boundvars. end( );
              ++ p )
      {
         if( p != val. boundvars. begin( ))
            stream << ",";
         stream << *p;
      }
      stream << " }. ";
   }

   stream << val. f;
   return stream;
}


